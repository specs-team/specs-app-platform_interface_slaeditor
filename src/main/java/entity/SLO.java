package entity;

import java.util.ArrayList;

import db.DBSlo;

public class SLO {
	
	private String idMetric;
	private String idSLO;
	private String operator;
	private String operand;
	private String importance;
	

	
	public SLO(){
		
	}
	
	public ArrayList<SLO> catturaSLO(String[] idMetric){
		int count=0;
		int campi=5;
		DBSlo dbSlo=new DBSlo();
		ArrayList<SLO> arrayListSLO=new ArrayList<SLO>();
		ArrayList arrayList=new ArrayList();
		
		arrayList=dbSlo.catturaSLO(idMetric);
		for(int i=0;i<arrayList.size();i=i+campi){
			SLO slo = new SLO();
			arrayListSLO.add(slo);
			arrayListSLO.get(count).idMetric=(String)arrayList.get(i);
			arrayListSLO.get(count).idSLO=(String)arrayList.get(i+1);
			arrayListSLO.get(count).operator=(String)arrayList.get(i+2);
			arrayListSLO.get(count).operand=(String)arrayList.get(i+3);
			arrayListSLO.get(count).importance=(String)arrayList.get(i+4);
			count++;
		}
		return arrayListSLO;
	}
	
	
	
	public String getIdMetric() {
		return idMetric;
	}
	public String getImportance() {
		return importance;
	}
	public void setImportance(String importance) {
		this.importance = importance;
	}

	public void setIdMetric(String idMetric) {
		this.idMetric = idMetric;
	}
	public String getIdSLO() {
		return idSLO;
	}
	public void setIdSLO(String idSLO) {
		this.idSLO = idSLO;
	}
	public String getOperator() {
		return operator;
	}
	public void setOperator(String operator) {
		this.operator = operator;
	}
	public String getOperand() {
		return operand;
	}
	public void setOperand(String operand) {
		this.operand = operand;
	}
	

}
