package action;

import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.LabelValueBean;


import entity.SecurityMetric;
import form.BrowseSecurityMetric_Form;




public class BrowseSecurityMetric_Action extends Action{
	public Collection smForm=new ArrayList();
	public ArrayList<SecurityMetric> arrayListSM= new ArrayList<SecurityMetric>();

	
	
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
	String returnMapping="";
	String  metricheSelezionate=null;
	
	
	SecurityMetric securityMetric=new SecurityMetric();
	arrayListSM=securityMetric.catturaSecurityMetric();
	
	smForm.clear();
	for(int i=0;i<arrayListSM.size();i++){
		smForm.add(new LabelValueBean(arrayListSM.get(i).getIdMetric(), arrayListSM.get(i).getIdMetric()));
	}
	request.setAttribute("smForm", smForm);
	returnMapping="success";
	
	
	if(form!=null){
	BrowseSecurityMetric_Form bsmForm=(BrowseSecurityMetric_Form)form;
	metricheSelezionate=bsmForm.getSelectedItems();
	//System.err.println(metricheSelezionate);
	
	
	String prova=null;
	if(metricheSelezionate!=null){
	for(int i=0;i<arrayListSM.size();i++){
		if(metricheSelezionate.compareTo(arrayListSM.get(i).getIdMetric())==0)
			prova=arrayListSM.get(i).getMetricDescription();
		}
	}
	if(prova!=null){
		System.err.println(prova);
//		String provaReplace;
//		
//		provaReplace=prova.replace("nist:", "nist");
//		System.out.println(prova);
		request.setAttribute("pippo","\""+StringEscapeUtils.escapeJava(prova)+"\"");
		returnMapping="update";
		
		
	}
	
	

	
	}
		return mapping.findForward(returnMapping);
	}
	
	
	

}