package action;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;

import xml.JDOMWriter;
import xml.XmlCapability;
import form.CreateSC_Finito_Form;
import entity.Capability;

public class CreateSC_Finito_Action extends Action {
	
	/**
	 * This method performs the override of the execute method of class Action and gives in output an ActionForward.
	 * It allow to login and save user in the session.   
	 *
	 * @param mapping        The ActionMapping.
	 * @param form        The ActionForm.
	 * @param request        The HttpServletRequest.
	 * @param response        The HttpServletResponse.      
	 * @return       The ActionForward
	 * @throws Exception the exception
	 */
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		System.out.println("Sono nella classe Create SC Finito Action");
		//JDOMWriter xmlWriter=JDOMWriter.getInstance();
		XmlCapability xmlCapability=XmlCapability.getInstance();
		String returnMapping="success";
		String idCapability = null;
		String  nameCapability=null;
		String descriptionCapability=null;
		if(form!=null){
			//effettuo il cast del form in ingresso
			CreateSC_Finito_Form createSCFintitoForm = (CreateSC_Finito_Form)form;
			idCapability=createSCFintitoForm.getIdCapability();
			nameCapability=createSCFintitoForm.getNameCapability();
			descriptionCapability=createSCFintitoForm.getDescriptionCapability();
			
			
			if(idCapability!=null){
				System.out.println(idCapability);
				System.out.println(nameCapability);
				System.out.println(descriptionCapability);
				xmlCapability.addCapabilityDescr(idCapability, nameCapability, descriptionCapability);
				System.err.println(xmlCapability.convertiXmlToString());
			}
			
			System.err.println(xmlCapability.convertiXmlToString());
			Capability capability = new Capability(nameCapability,xmlCapability.convertiXmlToString());
			returnMapping="finish";
		}
		
		//System.err.println(xmlWriter.convertiXML());
		
//		Client client=ClientBuilder.newClient();
//		WebTarget target = client.target("http://localhost:8080/service_manager_jaxrs/service_manager_rest_api").path("scs");
//		target.request(MediaType.TEXT_PLAIN).post(Entity.entity(xmlWriter.convertiXML(), MediaType.TEXT_PLAIN),String.class);
		//post(xmlWriter.convertiXML());
		
		
		
		
		return mapping.findForward(returnMapping);
	}

	 public void post(String stringXml) throws Exception{
	        HttpClient client = new DefaultHttpClient();
	        HttpPost post = new HttpPost("http://localhost:8080/service_manager_jaxrs/service_manager_rest_api/scs/");
	        HttpEntity entity = new ByteArrayEntity(stringXml.getBytes("UTF-8"));
	        post.setEntity(entity);
	        HttpResponse response = client.execute(post);
	        String result = EntityUtils.toString(response.getEntity());
	        System.out.println(result);
	    }
	
	
	
}