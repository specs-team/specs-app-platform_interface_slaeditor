<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>SPECS SLA editor	</title>

<link href="bootstrap/css/bootstrap.min.css"  type="text/css" rel="stylesheet">
<style type="text/css">
	body {
		padding-top: 60px;
		padding-bottom: 40px;
	}
	
	.sidebar-nav {
		padding: 9px 0;
	}
</style>

</head>
<!-- ********************************-->

<body>

<jsp:include page="../common/navigatorDeveloper.jsp">
<jsp:param  name="_home" value="class='active'"/>
</jsp:include>

    <!-- Main jumbotron for a primary marketing message or call to action -->
     <div class="panel">
      <div class="container">
        <h1>Associate one security metric with a security capability</h1>
        <h3>In this section, you can associate a security metric to a capability, and in particular to the security controls the Capability is composed of.
        <br/></h3>
        <h4>Please follow the next steps:</h4>
          <h4>1 - Select a Security Metric</h4>
          <html:form action="/AssociateSM_Action" >
	   			<logic:iterate name="smForm" id="qvalue">
	    			<big><html:radio styleClass="optradio" property="selectedItems" idName="qvalue" value="value"/>
	    			<bean:write name="qvalue" property="value" /></big>
	    			<br />
	    			</logic:iterate>
	    			<br />
				<html:submit title="Invia" />
			</html:form>
        
 		<h4><br />2 - Select a Security Capability</h4>
          <html:form action="/AssociateSM_Action" >
	   			<logic:iterate name="capabilityForm" id="qvalue2">
	    			<big><html:radio styleClass="optradio" property="selectedItems2" idName="qvalue2" value="value"/>
	    			<bean:write name="qvalue2" property="value" /></big>
	    			<br />
	    			
	    			</logic:iterate>
	    			<br />
				<html:submit title="Invia" />
			</html:form>          
          
      </div>
    </div>

<jsp:include page="../common/footer.jsp"/>

</body>
</html>