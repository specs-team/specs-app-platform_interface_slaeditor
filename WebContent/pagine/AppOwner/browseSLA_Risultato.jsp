<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>SPECS SLA editor</title>

<link href="bootstrap/css/bootstrap.min.css" type="text/css"
	rel="stylesheet">
<style type="text/css">
body {
	padding-top: 60px;
	padding-bottom: 40px;
}

.sidebar-nav {
	padding: 9px 0;
}
</style>

</head>
<!-- ********************************-->

<body ng-app="SPECS-APP-SLA-EDITOR">
<div id="_navigator">
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"> <img alt="Brand" src="img/specs-logo-32.png"></a>
			</div>
			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li><a href="<%=request.getContextPath()%>/HomeOwner_Action.do">Home</a></li>
					<li><a href="<%=request.getContextPath()%>/BrowseSLA_Action.do" >View SLA</a></li>
					<li><a href="<%=request.getContextPath()%>/DeleteSLA_Action.do">Delete SLA</a></li>
				
				</ul>
			</div>
			<!--/.nav-collapse -->
		</div>
	</nav>
</div>

	<!-- Main jumbotron for a primary marketing message or call to action -->
	<div class="container" ng-controller="slaEditContr">
		<div class="tab-pane">
			<div class="panel panel-default" ng-show="abstractSLA">
				<div class="panel-heading">
					<h1>Browse SLA Template</h1>
				</div>

				<div class="panel-body ">
					<div class="well" ng-show="abstractSLA">
						<b>SLA Template Name:</b>{{abstractSLA.wsagName}}
					</div>

					<div class="panel panel-default" ng-show="abstractSLA.Context">
						<div class="panel-body">
							<b>SLA Context</b> <br />
							<table class="table">
								<thead>
									<tr>
										<th width="30%">Attribute</th>
										<th width="30%">Value</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td><div>Agreement Initiator</div></td>
										<td><div>{{abstractSLA.Context.AgreementInitiator}}</div></td>
									</tr>
									<tr>
										<td><div>Agreement Responder</div></td>
										<td><div>{{abstractSLA.Context.AgreementResponder}}</div></td>
									</tr>
									<tr>
										<td><div>Expiration Time</div></td>
										<td><div>{{abstractSLA.Context.ExpirationTime}}</div></td>
									</tr>
									<tr>
										<td><div>Service Provider</div></td>
										<td><div>{{abstractSLA.Context.ServiceProvider}}</div></td>
									</tr>

								</tbody>
							</table>
						</div>
					</div>

					<div class="panel panel-default"
						ng-show="abstractSLA.Terms.All.ServiceDescriptionTerm.specsserviceDescription.specsservice_resources">
						<div class="panel-body">
							<b>SERVICE RESOURCE</b> <br />
							<table class="table">
								<thead>
									<tr>
										<th width="30%">Attribute</th>
										<th width="30%">Value</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td><div>Id</div></td>
										<td><div>{{abstractSLA.Terms.All.ServiceDescriptionTerm.specsserviceDescription.specsservice_resources.specsresources_provider.id}}</div></td>
									</tr>
									<tr>
										<td><div>Name</div></td>
										<td><div>{{abstractSLA.Terms.All.ServiceDescriptionTerm.specsserviceDescription.specsservice_resources.specsresources_provider.name}}</div></td>
									</tr>
									<tr>
										<td><div>Zone</div></td>
										<td><div>{{abstractSLA.Terms.All.ServiceDescriptionTerm.specsserviceDescription.specsservice_resources.specsresources_provider.zone}}</div></td>
									</tr>
									<tr>
										<td><div>VM appliance</div></td>
										<td><div>{{abstractSLA.Terms.All.ServiceDescriptionTerm.specsserviceDescription.specsservice_resources.specsresources_provider.specsVM.appliance}}</div></td>
									</tr>
									<tr>
										<td><div>VM description</div></td>
										<td><div>{{abstractSLA.Terms.All.ServiceDescriptionTerm.specsserviceDescription.specsservice_resources.specsresources_provider.specsVM.descr}}</div></td>
									</tr>
									<tr>
										<td><div>VM hardware</div></td>
										<td><div>{{abstractSLA.Terms.All.ServiceDescriptionTerm.specsserviceDescription.specsservice_resources.specsresources_provider.specsVM.hardware}}</div></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<!--  Capabilities no ARRAY -->
					<div class="panel panel-default" ng-show="!isCapabilityArray">
						<div class="panel-body">
							<b>CAPABILITIES</b><br />
							<table class="table">
								<thead>
									<tr>

									</tr>
								</thead>
								<tbody>
									<tr>
										<td><div>Name Capability</div></td>
										<td><div>{{abstractSLA.Terms.All.ServiceDescriptionTerm.specsserviceDescription.specscapabilities.specscapability.specsname}}</div></td>
									</tr>
									<tr>
										<td><div>Description Capability</div></td>
										<td><div>{{abstractSLA.Terms.All.ServiceDescriptionTerm.specsserviceDescription.specscapabilities.specscapability.specsdescription}}</div></td>
									</tr>
								</tbody>
							</table>
							<div class="panel-body" ng-show="!isCapabilityArray"
								ng-repeat="securityControl in abstractSLA.Terms.All.ServiceDescriptionTerm.specsserviceDescription.specscapabilities.specscapability.specssecurity_control">
								<div class="well">
									<table class="table">
										<thead>
											<br>
											<b>{{$index+1}} SECURITY CONTROL</b>

											<tr>
												<th width="30%">Attribute</th>
												<th width="30%">Value</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td><div>id</div></td>
												<td><div>{{securityControl.id}}</div></td>
											</tr>
											<tr>
												<td><div>Name</div></td>
												<td><div>{{securityControl.name}}</div></td>
											</tr>
											<tr>
												<td><div>Control family id</div></td>
												<td><div>{{securityControl.nistcontrol_family_id}}</div></td>
											</tr>
											<tr>
												<td><div>Control family name</div></td>
												<td><div>{{securityControl.nistcontrol_family_name}}</div></td>
											</tr>
											<tr>
												<td><div>Security control</div></td>
												<td><div>{{securityControl.nistsecurity_control}}</div></td>
											</tr>
											<tr>
												<td><div>Control enhancement</div></td>
												<td><div>{{securityControl.nistcontrol_enhancement}}</div></td>
											</tr>

										</tbody>
									</table>
									<div>
										<b>Description</b> <br />
										<div>{{securityControl.specscontrol_description}}</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!--  Capabilities  ARRAY -->

					<div class="panel panel-default" ng-show="isCapabilityArray">
						<div class="well">
							<b>CAPABILITIES</b><br />
							<br />
							<br />

							<div class="panel panel-default" ng-show="isCapabilityArray"
								ng-repeat="capability in abstractSLA.Terms.All.ServiceDescriptionTerm.specsserviceDescription.specscapabilities.specscapability">
								<div class="panel-body">
									<b>{{$index+1}} CAPABILITY</b><br />
									<table class="table">
										<thead>
											<tr>

											</tr>
										</thead>
										<tbody>
											<tr>
												<td><div>Name Capability</div></td>
												<td><div>{{capability.specsname}}</div></td>
											</tr>
											<tr>
												<td><div>Description Capability</div></td>
												<td><div>{{capability.specsdescription}}</div></td>
											</tr>
										</tbody>
									</table>
									<div class="panel-body" ng-show="isCapabilityArray"
										ng-repeat="securityControl in capability.specssecurity_control">
										<div class="well">
											<table class="table">
												<thead>
													<b>{{$index+1}} SECURITY CONTROL</b>
													<br />
													<tr>
														<th width="30%">Attribute</th>
														<th width="30%">Value</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td><div>id</div></td>
														<td><div>{{securityControl.id}}</div></td>
													</tr>
													<tr>
														<td><div>Name</div></td>
														<td><div>{{securityControl.name}}</div></td>
													</tr>
													<tr>
														<td><div>Control family id</div></td>
														<td><div>{{securityControl.nistcontrol_family_id}}</div></td>
													</tr>
													<tr>
														<td><div>Control family name</div></td>
														<td><div>{{securityControl.nistcontrol_family_name}}</div></td>
													</tr>
													<tr>
														<td><div>Security control</div></td>
														<td><div>{{securityControl.nistsecurity_control}}</div></td>
													</tr>
													<tr>
														<td><div>Control enhancement</div></td>
														<td><div>{{securityControl.nistcontrol_enhancement}}</div></td>
													</tr>

												</tbody>
											</table>
											<div>
												<b>Description</b> <br />
												<div>{{securityControl.specscontrol_description}}</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>



					<!--  ............. ...........................SERVICE PROPRIETIES (solo)......................................... -->
					<div class="panel panel-default"
						ng-show="!isServicesProprietiesArray">
						<div class="panel-body">
							<b>SERVICE PROPRIETIES</b><br />
							<table class="table">
								<tbody>
									<tr>
										<td><div>Name Service Property</div></td>
										<td><div>{{abstractSLA.Terms.All.ServicesProprieties.wsagName}}</div></td>
									</tr>
									<tr>
										<td><div>Description Service Property</div></td>
										<td><div>{{abstractSLA.Terms.All.ServicesProprieties.wsagServiceName}}</div></td>
									</tr>
								</tbody>
							</table>
							<!--  ............. ...........................variable solo.................-->
							<div class="panel panel-default">
								<div class="well">
									<table class="table">
										<thead>
											<th width="30%">Attribute</th>
											<th width="30%">Value</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td><div>Name</div></td>
												<td><div>{{abstractSLA.Terms.All.ServicesProprieties.VariableSet.Variable.wsagName}}</div></td>
											</tr>
											<tr>
												<td><div>Metric</div></td>
												<td><div>{{abstractSLA.Terms.All.ServicesProprieties.VariableSet.Variable.wsagMetric}}</div></td>
											</tr>
											<tr>
												<td><div>Location</div></td>
												<td><div>{{abstractSLA.Terms.All.ServicesProprieties.VariableSet.Variable.Location}}</div></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>

							<!--  ............. ...........................variable (array).................-->
							<!-- <div class="panel panel-default"
										ng-show="!isServicesProprietiesArray"
										ng-repeat="variable in abstractSLA.Terms.All.ServicesProprieties.VariableSet.Variable">
										<div class="panel">
											<table class="table">
												<thead>
													<th width="30%">Attribute</th>
													<th width="30%">Value</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td><div>Name</div></td>
														<td><div>{{variable.wsagName}}</div></td>
													</tr>
													<tr>
														<td><div>Metric</div></td>
														<td><div>{{variable.wsagName}}</div></td>
													</tr>
													<tr>
														<td><div>Location</div></td>
														<td><div>{{variable.Location}}</div></td>
													</tr>
												</tbody>
											</table>
										</div>
									</div> -->


						</div>
					</div>
				</div>
			</div>

			<!--  ............. ........................SERVICE PROPRIETIES (array)......................................... -->

			<div class="panel panel-default" ng-show="isServicesProprietiesArray">
				<div class="panel-body">
					<b>SERVICE PROPRIETIES</b><br />
					<div class="panel-body" ng-show="isServicesProprietiesArray"
						ng-repeat="servicesProprieties in abstractSLA.Terms.All.ServicesProprieties">
						<div class="well">
							<table class="table">
								<tbody>
									<tr>
										<td><div>Name Service Property</div></td>
										<td><div>{{servicesProprieties.wsagName}}</div></td>
									</tr>
									<tr>
										<td><div>Description Service Property</div></td>
										<td><div>{{servicesProprieties.wsagServiceName}}</div></td>
									</tr>
								</tbody>
							</table>

							<div class="panel panel-default"
								ng-show="isServicesProprietiesArray"
								ng-repet="servicesProprieties in abstractSLA.Terms.All.ServicesProprieties">
								<div class="panel">
									<table class="table">
										<thead>
											<th width="30%">Attribute</th>
											<th width="30%">Value</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td><div>Name</div></td>
												<td><div>{{servicesProprieties.VariableSet.Variable.wsagName}}</div></td>
											</tr>
											<tr>
												<td><div>Metric</div></td>
												<td><div>{{servicesProprieties.VariableSet.Variable.wsagMetric}}</div></td>
											</tr>
											<tr>
												<td><div>Location</div></td>
												<td><div>{{servicesProprieties.VariableSet.Variable.Location}}</div></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>





			<!--  ............. ........................GUARANTEE TERM  (solo)......................................... -->
			<div class="panel panel-default" ng-show="!isGuaranteeTermArray">
				<div class="panel-body">
					<b>GUARANTEE TERM</b><br />


					<table class="table">
						<tbody>
							<tr>
								<td><div>Name Guarantee Term</div></td>
								<td><div>{{abstractSLA.Terms.All.GuaranteeTerm.wsagName}}</div></td>
							</tr>
							<tr>
								<td><div>Description Guarantee Term</div></td>
								<td><div>{{abstractSLA.Terms.All.GuaranteeTerm.wsagObligated}}</div></td>
							</tr>
						</tbody>
					</table>
					<div class="panel panel-default" ng-show="!isGuaranteeTermArray">
						<div class="well">
							<table class="table">
								<thead>
									<th width="30%">Attribute</th>
									<th width="30%">Value</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td><div>SLO Name</div></td>
										<td><div>{{abstractSLA.Terms.All.GuaranteeTerm.ServiceLevelObjective.CustomServiceLevel.objectiveList.specsSLO.SLO_ID}}</div></td>
									</tr>
									<tr>
										<td><div>SLO Metric Ref</div></td>
										<td><div>{{abstractSLA.Terms.All.GuaranteeTerm.ServiceLevelObjective.CustomServiceLevel.objectiveList.specsSLO.specsMetricREF}}</div></td>
									</tr>
									<tr>
										<td><div>SLO Operator</div></td>
										<td><div>{{abstractSLA.Terms.All.GuaranteeTerm.ServiceLevelObjective.CustomServiceLevel.objectiveList.specsSLO.specsSLOexpression.specsoneOpExpression.operator}}</div></td>
									</tr>
									<tr>
										<td><div>SLO Operand</div></td>
										<td><div>{{abstractSLA.Terms.All.GuaranteeTerm.ServiceLevelObjective.CustomServiceLevel.objectiveList.specsSLO.specsSLOexpression.specsoneOpExpression.operand}}</div></td>
									</tr>
									<tr>
										<td><div>SLO importance weigth</div></td>
										<td><div>{{abstractSLA.Terms.All.GuaranteeTerm.ServiceLevelObjective.CustomServiceLevel.objectiveList.specsSLO.specsSLOexpression.specsimportance_weigth}}</div></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>




			<!--  ............. .......................GUARANTEE TERM  (array)......................................... -->

			<div class="panel panel-default" ng-show="isGuaranteeTermArray">
				<div class="panel-body">
					<b>GUARANTEE TERM</b><br />
					<div class="panel-body" ng-repeat="guaranteeTerm in abstractSLA.Terms.All.GuaranteeTerm">
								<div class="well">
								<table class="table">
								<tbody>
									<tr>
										<td><div>Name Guarantee Term</div></td>
										<td><div>{{guaranteeTerm.wsagName}}</div></td>
									</tr>
									<tr>
										<td><div>Description Guarantee Term</div></td>
										<td><div>{{guaranteeTerm.wsagObligated}}</div></td>
									</tr>
								</tbody>
							</table>
 							
 							<div class="panel panel-default" ng-show="isGuaranteeTermArray">
										<div class="wel">
											<table class="table">
												<thead>
													<th width="30%">Attribute</th>
													<th width="30%">Value</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td><div>SLO Name</div></td>
														<td><div>{{guaranteeTerm.ServiceLevelObjective.CustomServiceLevel.objectiveList.specsSLO.SLO_ID}}</div></td>
													</tr>
													<tr>
														<td><div>SLO Metric Ref</div></td>
														<td><div>{{guaranteeTerm.ServiceLevelObjective.CustomServiceLevel.objectiveList.specsSLO.specsMetricREF}}</div></td>
													</tr>
													<tr>
														<td><div>SLO Operator</div></td>
														<td><div>{{guaranteeTerm.ServiceLevelObjective.CustomServiceLevel.objectiveList.specsSLO.specsSLOexpression.specsoneOpExpression.operator}}</div></td>
													</tr>
													<tr>
														<td><div>SLO Operand</div></td>
														<td><div>{{guaranteeTerm.ServiceLevelObjective.CustomServiceLevel.objectiveList.specsSLO.specsSLOexpression.specsoneOpExpression.operand}}</div></td>
													</tr>
													<tr>
														<td><div>SLO importance weigth</div></td>
														<td><div>{{guaranteeTerm.ServiceLevelObjective.CustomServiceLevel.objectiveList.specsSLO.specsSLOexpression.specsimportance_weigth}}</div></td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
				</div>
			</div>


			


		</div>
</div>



	<jsp:include page="../common/footer.jsp" />


	<script src="<%=request.getContextPath()%>/js/angular.js"></script>
	<!-- *************************** -->
	<script src="<%=request.getContextPath()%>/js/jquery.min.js"></script>
	<script src="<%=request.getContextPath()%>/js/jquery.xpath.js"></script>
	<script src="<%=request.getContextPath()%>/js/jquery.xml2json.js"></script>

	<script
		src="<%=request.getContextPath()%>/js/bootstrap_wizard/jquery.bootstrap.wizard.js"></script>
	
	<!-- Page constructors -->
	<!-- ANGULAR SCRIPT -->
	<script>
		var app = angular.module('SPECS-APP-SLA-EDITOR', []);

		app.controller('slaEditContr', [ '$scope', function($scope) {
			console.log("chiamata inizializzazione angular");
			$scope.metricUserName = "";
			
			$scope.xmlRetrieved = unescape(${pippo});
			console.log($scope.xmlRetrieved);
			
			
			$scope.slaTemplate = $.parseXML($scope.xmlRetrieved);
		    $scope.abstractSLA = $.xml2json($scope.slaTemplate);
		    //findAndReplace($scope.abstractCapability,'nist','nist:');
		    /* findAndReplace($scope.abstractCapability,'nist:','nist'); */
		    //findAndReplace($scope.abstractCapability,"nist","nist:");
		    //findAndReplace($scope.abstractCapability,"nist:","nist");
		  
		    console.log($scope.abstractSLA);
		     
		     if ($scope.abstractSLA.Terms.All.ServiceDescriptionTerm.specsserviceDescription.specscapabilities.specscapability instanceof Array) {
					$scope.isCapabilityArray = true;
				} else {
					$scope.isRuleDefinitionArray = false;
				}  
		    
		     if ($scope.abstractSLA.Terms.All.ServiceDescriptionTerm.specsserviceDescription.specscapabilities.specscapability.specssecurity_control instanceof Array) {
					$scope.isSecurityControlArray = true;
				} else {
					$scope.isRuleDefinitionArray = false;
				}
		     
		     if ($scope.abstractSLA.Terms.All.ServicesProprieties instanceof Array) {
					$scope.isServicesProprietiesArray = true;
				} else {
					$scope.isRuleDefinitionArray = false;
				} 

		      if ($scope.abstractSLA.Terms.All.GuaranteeTerm instanceof Array) {
					$scope.isGuaranteeTermArray = true;
				} else {
					$scope.isRuleDefinitionArray = false;
				}  
				
		
		     
		     
		   
		    
		} ]);
	</script>
</body>
</html>