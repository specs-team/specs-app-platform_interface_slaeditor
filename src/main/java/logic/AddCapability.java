package logic;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import xml.JDOMWriter;
import entity.Capability;
import entity.Controllo;
import entity.MappingCapMetric;
import entity.SLO;
import db.DBCapability;
public class AddCapability {
	
	private ArrayList<Controllo> arrayListControllo=new ArrayList<>();
	private String [] capabilitySelezionate={};
	private String capabilityXML;
	
	
	
	public AddCapability(String[] capabilitySelezionate)throws IOException, ParserConfigurationException, SAXException{
		this.capabilitySelezionate=capabilitySelezionate;
		JDOMWriter xmlWriter=JDOMWriter.getInstance();
		ArrayList<Capability> arrayListCapability= new ArrayList<Capability>();
		Capability capability=new Capability();
		arrayListCapability=capability.catturaCapability();
		
		xmlWriter.salvaNomeControlli(capabilitySelezionate);
		int j=0;
		while( j!=capabilitySelezionate.length){
		for(int i=0;i<arrayListCapability.size();i++){
			if(capabilitySelezionate[j].compareTo(arrayListCapability.get(i).getIdCapability())==0)
				capabilityXML=arrayListCapability.get(i).getCapabilityDescription();
			}
		arrayListControllo=costruisciControlli(capabilityXML);
		
		//stampaArrayList();
		
		//aggiungo la parte delle capabilities
		xmlWriter.salvaXMLcapability(arrayListControllo,capabilitySelezionate[j]);
		
		//aggiungo la parte delle ServiceProprietis
		ArrayList<MappingCapMetric> arrayListMapping=new ArrayList<MappingCapMetric>();
		MappingCapMetric mapping= new MappingCapMetric();
		arrayListMapping=mapping.catturaSecurityMetricSingolaCapability(capabilitySelezionate[j]);
		
		xmlWriter.creaServiceProperties(capabilitySelezionate[j],arrayListMapping);
		
		//aggiungo la parte delle GuaranteeTerm

			
		ArrayList<SLO> arrayListSlo=new ArrayList<SLO>();
		SLO slo=new SLO();
		
		String[] querySlo=new String[arrayListMapping.size()];
		for(int i=0;i<arrayListMapping.size();i++){
			querySlo[i]=arrayListMapping.get(i).getMetrica();
		}
		
		
		arrayListSlo=slo.catturaSLO(querySlo);
		xmlWriter.creaGT(capabilitySelezionate[j],arrayListSlo);
		System.err.println(xmlWriter.convertiXML());

		
		arrayListControllo.clear();
		j++;
		}
	}
	
	
	
	
	
	
	public ArrayList<Controllo> costruisciControlli(String capabilityXML)throws IOException, ParserConfigurationException, SAXException{
		
		String idControl=null;
		String controlName=null;
		String familyId=null;
		String familyName=null;
		int control=0;
		int enhancement=0;
		String description=null;
		
		DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		InputSource is = new InputSource();
		is.setCharacterStream(new StringReader(capabilityXML));
		Document doc=db.parse(is);
		Node root=doc.getFirstChild();
		int count =0;
		for(int i=0;i<root.getChildNodes().getLength();i++){
			Node securityControl=root.getChildNodes().item(i);
			description=securityControl.getTextContent();
			
			if(securityControl.hasAttributes()){
				idControl=securityControl.getAttributes().item(0).getNodeValue();
				controlName=securityControl.getAttributes().item(1).getNodeValue();
				familyId=securityControl.getAttributes().item(3).getNodeValue();
				familyName=securityControl.getAttributes().item(4).getNodeValue();
				try {
					control = Integer.parseInt(securityControl.getAttributes().item(5).getNodeValue());
					enhancement = Integer.parseInt(securityControl.getAttributes().item(2).getNodeValue());
		 			} catch (Exception e) { 
		 				e.printStackTrace(); 
		 			}
				Controllo controllo=new Controllo();
				arrayListControllo.add(controllo);
				arrayListControllo.get(count).setIdControl(idControl);
				arrayListControllo.get(count).setControlName(controlName);
				arrayListControllo.get(count).setFamilyId(familyId);
				arrayListControllo.get(count).setFamilyName(familyName);
				arrayListControllo.get(count).setControl(control);
				arrayListControllo.get(count).setEnhancement(enhancement);
				arrayListControllo.get(count).setDescription(description);
				count++;
			}
			
			
		}
		return arrayListControllo;
			}
	
	public void stampaArrayList(){
		for(int i=0;i<arrayListControllo.size();i++){
			System.out.println("Stampo id "+arrayListControllo.get(i).getIdControl());
			System.out.println("Stampo name "+arrayListControllo.get(i).getControlName());
			System.out.println("Stampo familyId "+arrayListControllo.get(i).getFamilyId());
			System.out.println("Stampo familyName "+arrayListControllo.get(i).getFamilyName());
			System.out.println("Stampo control "+arrayListControllo.get(i).getControl());
			System.out.println("Stampo enhancement "+arrayListControllo.get(i).getEnhancement());
			System.out.println("Stampo la descrizione "+arrayListControllo.get(i).getDescription());
		}
	}
	
	}

