package action;

import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.LabelValueBean;

 

import db.DBMappingCapMetric;
import entity.MappingCapMetric;
 
import form.DeleteMapping_Form;

public class DeleteMapping_Action extends Action{
	
	public Collection mappingForm=new ArrayList();
	public ArrayList<MappingCapMetric> arrayListMapping= new ArrayList<MappingCapMetric>();
	public ArrayList<MappingCapMetric> arrayListMappingpulito= new ArrayList<MappingCapMetric>();
	
	
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
	String returnMapping="";
	String  mappingSelezionato=null;
	MappingCapMetric mapp=new MappingCapMetric();
	arrayListMapping=mapp.catturaAllMapping();
	
	mappingForm.clear();
	for(int i=0;i<arrayListMapping.size();i++){
		mappingForm.add(new LabelValueBean("Id Mapping "+arrayListMapping.get(i).getIdMapping() , arrayListMapping.get(i).getIdMapping()));
		}
	 
	request.setAttribute("mappingForm", mappingForm);
	returnMapping="success";
	
	
	if(form!=null){
		DeleteMapping_Form deleteMapping=(DeleteMapping_Form)form;
		mappingSelezionato=deleteMapping.getSelectedItems();
	    System.err.println(mappingSelezionato);
	
	
 	     
	    if(mappingSelezionato!=null){
	    	DBMappingCapMetric dbMapping= new DBMappingCapMetric();
	    	dbMapping.eliminaMapping(mappingSelezionato);
	    	 
	    	}

	    
	}
		
	 
	
	

	
 
		return mapping.findForward("success");
	}
	
	
	

}