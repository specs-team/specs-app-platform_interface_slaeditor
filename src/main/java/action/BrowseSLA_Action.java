package action;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.LabelValueBean;

import entity.Wsag;
import form.BrowseSLA_Form;


public class BrowseSLA_Action extends Action {
	
	private Collection wsagForm =new ArrayList();
	private ArrayList<Wsag> arrayListWsag=new ArrayList<Wsag>();
	
	/**
	 * This method performs the override of the execute method of class Action and gives in output an ActionForward.
	 * It allow to login and save user in the session.   
	 *
	 * @param mapping        The ActionMapping.
	 * @param form        The ActionForm.
	 * @param request        The HttpServletRequest.
	 * @param response        The HttpServletResponse.      
	 * @return       The ActionForward
	 * @throws Exception the exception
	 */
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		String wsagSelezionati=null;
		String returnMapping="";
		Wsag wsag=new Wsag();
		arrayListWsag=wsag.catturaWsag();
		
		wsagForm.clear();
		for(int i=0;i<arrayListWsag.size();i++){
			wsagForm.add(new LabelValueBean("Wsag "+arrayListWsag.get(i).getIdWsag() , arrayListWsag.get(i).getIdWsag()));
		}
		request.setAttribute("wsagForm", wsagForm);
		returnMapping="success";
		
		if(form!=null){
			BrowseSLA_Form browseSLAForm=(BrowseSLA_Form)form;
			wsagSelezionati=browseSLAForm.getSelectedItems();
			System.err.println(wsagSelezionati);
			String prova=null;
			if(wsagSelezionati!=null){
				for(int i=0;i<arrayListWsag.size();i++){
					if(wsagSelezionati.compareTo(arrayListWsag.get(i).getIdWsag())==0)
						prova=arrayListWsag.get(i).getWsagDescription();
				}
			}
			if(prova!=null){
//				response.setContentType("text/xml");
//				PrintWriter out=response.getWriter();
//				prova=prova.replace("£", "'");
//				out.println(prova);
//				out.close();
//				
//				request.setCharacterEncoding("text/xml");
//				prova = prova.replace("\"", "\\\"");
//				System.out.println(prova);
//				request.setAttribute("pippo","\""+StringEscapeUtils.escapeJava(prova)+"\"");
//				returnMapping="update";
				String provaReplace;
				prova=prova.replace("£", "'");
				
				provaReplace=prova.replace("nist:", "nist");
				
				provaReplace=provaReplace.replace("wsag:Name", "wsagName");
				provaReplace=provaReplace.replace("wsag:ServiceName", "wsagServiceName");
				provaReplace=provaReplace.replace("wsag:Obligated", "wsagObligated");
				provaReplace=provaReplace.replace("wsag:Metric", "wsagMetric");
				provaReplace=provaReplace.replace("specs:", "specs");
				
				request.setAttribute("pippo","\""+StringEscapeUtils.escapeJava(provaReplace)+"\"");
				returnMapping="update";
				
				
				
			}
		}
		
		
		return mapping.findForward(returnMapping);
	}

}
