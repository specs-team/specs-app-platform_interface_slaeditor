package form;

import org.apache.struts.action.ActionForm;

public class CreateSC_Finito_Form extends ActionForm{
	
	
	 private String nome=null;
     
	    // definisce le proprieta' da scambiare con il form
	    public void setNome(String nome) { this.nome=nome; }
	    public String getNome() { return(this.nome); }
	    
	    private String idCapability=null;
		private String nameCapability=null;
		private String descriptionCapability=null;
		
		
		public void setIdCapability(String idCapability){
			this.idCapability=idCapability;
		}
		public String getIdCapability(){
			return this.idCapability;
		}
		public void setNameCapability(String nameCapability){
			this.nameCapability=nameCapability;
		}
		public String getNameCapability(){
			return this.nameCapability;
		}
		public void setDescriptionCapability(String descriptionCapability){
			this.descriptionCapability=descriptionCapability;
		}
		public String getDescriptionCapability(){
			return this.descriptionCapability;
		}

}
