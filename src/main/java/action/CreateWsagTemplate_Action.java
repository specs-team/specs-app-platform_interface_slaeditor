package action;

import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import logic.WsagTemplate;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.LabelValueBean;

import form.CreateWsagTemplate_Form;

public class CreateWsagTemplate_Action extends Action {
	
	public Collection serviceProprities=new ArrayList();
	public Collection guaranteeTerm=new ArrayList();
	
	/**
	 * This method performs the override of the execute method of class Action and gives in output an ActionForward.
	 * It allow to login and save user in the session.   
	 *
	 * @param mapping        The ActionMapping.
	 * @param form        The ActionForm.
	 * @param request        The HttpServletRequest.
	 * @param response        The HttpServletResponse.      
	 * @return       The ActionForward
	 * @throws Exception the exception
	 */
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		System.out.println("Sono nella classe Create Wsag Template Action");
		
		serviceProprities.clear();
		serviceProprities.add(new LabelValueBean("ServiceProprieties WebPool", "SPWebPool"));
		serviceProprities.add(new LabelValueBean("ServiceProprieties Sva", "SPSva"));
		serviceProprities.add(new LabelValueBean("ServiceProprieties OpenVas", "SPOpenVas"));
		serviceProprities.add(new LabelValueBean("ServiceProprieties Ossec", "SPOssec"));
		guaranteeTerm.clear();
		guaranteeTerm.add(new LabelValueBean("GuaranteeTerm WebPool", "GTWebPool"));
		guaranteeTerm.add(new LabelValueBean("GuaranteeTerm Sva", "GTSva"));
		guaranteeTerm.add(new LabelValueBean("GuaranteeTerm OpenVas", "GTOpenVas"));
		guaranteeTerm.add(new LabelValueBean("GuaranteeTerm Ossec", "GTOssec"));
		request.setAttribute("serviceProprities", serviceProprities);
		request.setAttribute("guaranteeTerm", guaranteeTerm);
		
		String[] spSelezionati = null;
		String[] gtSelezionati = null;
		
		if(form!=null){
			CreateWsagTemplate_Form createWsagTemplate = (CreateWsagTemplate_Form)form;
			spSelezionati=createWsagTemplate.getSelectedItems1();
			gtSelezionati=createWsagTemplate.getSelectedItems2();
			
			if(spSelezionati!=null){
				for(int i=0;i<spSelezionati.length;i++)
					System.out.println(spSelezionati[i]);
				WsagTemplate wsagTemplate=new WsagTemplate();
				wsagTemplate.salvaSP(spSelezionati);
				wsagTemplate.addServiceProprieties();
				
			}
			if(gtSelezionati!=null){
				for(int i=0;i<gtSelezionati.length;i++)
					System.out.println(gtSelezionati[i]);
				WsagTemplate wsagTemplate=new WsagTemplate();
				wsagTemplate.salvaGT(gtSelezionati);
				wsagTemplate.addGuaranteeTerms();
				
			}
			
		}
		
		
		
		
		return mapping.findForward("success");
	}

}