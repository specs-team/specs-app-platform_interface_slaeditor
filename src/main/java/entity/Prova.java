package entity;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import xml.JDOMWriter;

public class Prova {
	
	private static String[] metrichePulite={};
	private ArrayList arrayListMetriche;
	
	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
		//arrayMetrica=null;
		String[]capabilities={"Weeeeb","System integrity"};
		
		ArrayList<MappingCapMetric> arrayListMapping=new ArrayList<MappingCapMetric>();
		ArrayList<MappingCapMetric> arrayListMappingPulito=new ArrayList<MappingCapMetric>();
		
		MappingCapMetric mapping= new MappingCapMetric();
		arrayListMapping=mapping.catturaSecurityMetric(capabilities);

		ArrayList<SecurityMetric> arrayListMetric=new ArrayList<SecurityMetric>();
		SecurityMetric securityMetric=new SecurityMetric();
		arrayListMetric=securityMetric.catturaSecurityMetric();
		
//		for(int i=0;i<arrayListMapping.size();i++){
//			System.out.println(arrayListMapping.get(i).getCapability());
//			System.out.println(arrayListMapping.get(i).getControllo());
//			System.out.println(arrayListMapping.get(i).getMetrica());
//		}
		
//		for(int i=0;i<arrayListMetric.size();i++){
//			System.err.println(arrayListMetric.get(i).getIdMetric());
//			System.err.println(arrayListMetric.get(i).getMetricDescription());
//		}
		
		arrayListMappingPulito=pulisciMetriche(arrayListMapping);
//		for(int i=0;i<arrayListMappingPulito.size();i++){
//			System.out.println(arrayListMappingPulito.get(i).getCapability());
//			System.out.println(arrayListMappingPulito.get(i).getControllo());
//			System.out.println(arrayListMappingPulito.get(i).getMetrica());
//		}
		
//		for(int i=0;i<arrayListMappingPulito.size();i++){
//			for(int j=0;j<arrayListMetric.size();j++){
//				if(arrayListMappingPulito.get(i).getMetrica().compareTo(arrayListMetric.get(j).getIdMetric())==0){
//					System.err.println(arrayListMetric.get(j).getMetricDescription());
//				}
//			}
//		}
		
//		ArrayList<SecurityMetric> arrayLsecurityMetric=new ArrayList<SecurityMetric>();
//		arrayLsecurityMetric=parsingSecurityMetric(arrayListMetric.get(0).getMetricDescription());
//		
//		JDOMWriter xmlWriter=JDOMWriter.getInstance();
//		xmlWriter.configBase();
//		xmlWriter.salvaSecurityMetric(arrayLsecurityMetric);
//		//System.err.println(xmlWriter.convertiXML());
//		arrayLsecurityMetric=parsingSecurityMetric(arrayListMetric.get(1).getMetricDescription());
//		xmlWriter.salvaSecurityMetric(arrayLsecurityMetric);
//		
//		
//		ArrayList<MappingCapMetric> arrayListMapping2=new ArrayList<MappingCapMetric>();
//		MappingCapMetric mappingCapMetric2=new MappingCapMetric();
//		
//		arrayListMapping2=mappingCapMetric2.catturaSecurityMetricSingola("System integrity");
//		xmlWriter.creaServiceProperties("System integrity",arrayListMapping2);
//		for(int i=0;i<arrayListMapping2.size();i++){
//		System.out.println(arrayListMapping2.get(i).getCapability());
//		System.out.println(arrayListMapping2.get(i).getControllo());
//		System.out.println(arrayListMapping2.get(i).getMetrica());
//		}System.out.println(xmlWriter.convertiXML());
		
		SLO slo=new SLO();
		ArrayList<SLO> arrayListSlo=new ArrayList<SLO>();
		String [] prova={"Level of Diversity", "Level of Redundancy"};
		arrayListSlo=slo.catturaSLO(prova);
		for(int i=0;i<arrayListSlo.size();i++){
		System.out.println(arrayListSlo.get(i).getIdMetric());
		System.out.println(arrayListSlo.get(i).getIdSLO());
		System.out.println(arrayListSlo.get(i).getOperand());
		System.out.println(arrayListSlo.get(i).getOperator());
		System.out.println(arrayListSlo.get(i).getImportance());
		}
		
		
		
		
		
	
	}
	
	public static ArrayList<MappingCapMetric> pulisciMetriche(ArrayList<MappingCapMetric> arrayListMapping ){
		ArrayList<MappingCapMetric> arrayListMappingPulito=new ArrayList<MappingCapMetric>();
		int n=arrayListMapping.size();
		int j=0;
		for(int i=0;i<n;i++){
			boolean esiste=false;
			for(int k=i+1;k<n;k++){
				if(arrayListMapping.get(i).getMetrica().compareTo(arrayListMapping.get(k).getMetrica())==0 && esiste==false){
					esiste=true;
				}
			}
			if(esiste==false){
				//arrayMetrica[j]=arrayMetrica[i];
				j++;
				arrayListMappingPulito.add(arrayListMapping.get(i));
				;
			}
		}
		return arrayListMappingPulito;
	}
	
	public static ArrayList<SecurityMetric> parsingSecurityMetric(String prova) throws ParserConfigurationException, SAXException, IOException{
		ArrayList<SecurityMetric> arrayLsecurityMetric=new ArrayList<SecurityMetric>();
		SecurityMetric sm=new SecurityMetric();
		DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		InputSource is = new InputSource();
		is.setCharacterStream(new StringReader(prova));
		Document doc=db.parse(is);
		Node root=doc.getFirstChild();
		//System.out.println(root.getNodeName());
		if(root.hasAttributes()){
			arrayLsecurityMetric.add(sm);
			arrayLsecurityMetric.get(0).setName(root.getAttributes().item(0).getNodeValue());
			arrayLsecurityMetric.get(0).setReferenceId(root.getAttributes().item(1).getNodeValue());
//			System.out.println(root.getAttributes().item(0).getNodeValue());
//			System.out.println(root.getAttributes().item(1).getNodeValue());
		}
			Node metricDefinition=root.getChildNodes().item(1);
			//System.out.println(metricDefinition.getNodeName());
			Node metricRules=root.getChildNodes().item(3);
			//System.out.println(metricRules.getNodeName());
			Node metricParameters=root.getChildNodes().item(5);
			//System.out.println(metricParameter.getNodeName());
			
			Node unit =metricDefinition.getChildNodes().item(1);
			if(unit.hasAttributes()){
				//System.out.println(unit.getAttributes().item(0).getNodeValue());
				arrayLsecurityMetric.get(0).setUnitName(unit.getAttributes().item(0).getNodeValue());
			}
			Node intervalUnit=unit.getChildNodes().item(3);
//			System.out.println(intervalUnit.getChildNodes().item(1).getTextContent());
//			System.out.println(intervalUnit.getChildNodes().item(3).getTextContent());
//			System.out.println(intervalUnit.getChildNodes().item(5).getTextContent());
//			System.out.println(intervalUnit.getChildNodes().item(7).getTextContent());
			arrayLsecurityMetric.get(0).setIntervalType(intervalUnit.getChildNodes().item(1).getTextContent());
			arrayLsecurityMetric.get(0).setIntervalStart(intervalUnit.getChildNodes().item(3).getTextContent());
			arrayLsecurityMetric.get(0).setIntervalStop(intervalUnit.getChildNodes().item(5).getTextContent());
			arrayLsecurityMetric.get(0).setIntervalSpep(intervalUnit.getChildNodes().item(7).getTextContent());
			
			
			Node scale =metricDefinition.getChildNodes().item(3);
			//System.out.println(scale.getChildNodes().item(1).getTextContent());
			arrayLsecurityMetric.get(0).setScale(scale.getChildNodes().item(1).getTextContent());
			Node expression =metricDefinition.getChildNodes().item(5);
			//System.out.println(expression.getTextContent());
			arrayLsecurityMetric.get(0).setEspression(expression.getTextContent());
			Node definition =metricDefinition.getChildNodes().item(7);
			//System.out.println(definition.getTextContent());
			arrayLsecurityMetric.get(0).setDefinition(definition.getTextContent());
			
			
			Node metricRule= metricRules.getChildNodes().item(1);
			if(metricRule.hasAttributes()){
				//System.out.println(metricRule.getAttributes().item(0).getNodeValue());
				arrayLsecurityMetric.get(0).setRuleId(metricRule.getAttributes().item(0).getNodeValue());
			}
			//System.out.println(metricRule.getChildNodes().item(1).getTextContent());
			arrayLsecurityMetric.get(0).setRuleDescription(metricRule.getChildNodes().item(1).getTextContent());
			//System.out.println(metricRule.getChildNodes().item(3).getTextContent());
			arrayLsecurityMetric.get(0).setRuleValue(metricRule.getChildNodes().item(3).getTextContent());;
			
			if(metricParameters.hasAttributes()){
				//System.out.println(metricParameters.getAttributes().item(0).getNodeValue());
				arrayLsecurityMetric.get(0).setParamId(metricParameters.getAttributes().item(0).getNodeValue());
			}
			//System.out.println(metricParameters.getChildNodes().item(1).getTextContent());
			arrayLsecurityMetric.get(0).setParamDescription(metricParameters.getChildNodes().item(1).getTextContent());
			//System.out.println(metricParameters.getChildNodes().item(3).getTextContent());
			arrayLsecurityMetric.get(0).setParamType(metricParameters.getChildNodes().item(3).getTextContent());
			//System.out.println(metricParameters.getChildNodes().item(5).getTextContent());
			arrayLsecurityMetric.get(0).setParamValue(metricParameters.getChildNodes().item(5).getTextContent());
			
		return arrayLsecurityMetric;
	}

	
	
}