package entity;
import java.util.ArrayList;

import db.DBCapability;

public class Capability {
	
	String idCpability;
	String capabilityDescription;

	
	
	
	public Capability(){
	}
	
	public Capability(String idCapability, String capabilityDescription){
		this.idCpability=idCapability;
		this.capabilityDescription=capabilityDescription;
		DBCapability dbCapability= new DBCapability(idCapability,capabilityDescription);
	}
	
	public ArrayList<Capability> catturaCapability(){
		int count=0;
		int campi=2;
		DBCapability dbCapability=new DBCapability();
		ArrayList<Capability> arrayListCapability= new ArrayList<Capability>();
		ArrayList arrayList= new ArrayList();
		arrayList=dbCapability.catturaCapability();
		for(int i=0;i<arrayList.size();i=i+campi){
			Capability capability=new Capability();
			arrayListCapability.add(capability);
			arrayListCapability.get(count).idCpability=(String)arrayList.get(i);
			arrayListCapability.get(count).capabilityDescription=(String)arrayList.get(i+1);
			count++;
		}
		return arrayListCapability;
	}
	
	public String getIdCapability(){
		return this.idCpability;
	}
	
	public String getCapabilityDescription(){
		return this.capabilityDescription;
	}
}
