package form;

import org.apache.struts.action.ActionForm;


public class Login_Form extends ActionForm{
	
	private String nome=null;
	private String pass=null;

	public void setNome(String nome) {
		this.nome=nome; 
	}

	public String getNome() { 
		return(this.nome); 
	}	
	
	
	public void setPass(String pass) {
		this.pass=pass; 
	}

	public String getPass() { 
		return(this.pass); 
	}	
	
	
	private String selectedItems =null;
	
	
	public String getSelectedItems(){
		return selectedItems;
	}
	
	public void setSelectedItems(String selectedItems){
		this.selectedItems=selectedItems;
	}


}