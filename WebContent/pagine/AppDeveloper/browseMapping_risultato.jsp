<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>SPECS SLA editor</title>

<link href="bootstrap/css/bootstrap.min.css" type="text/css"
	rel="stylesheet">
<style type="text/css">
body {
	padding-top: 60px;
	padding-bottom: 40px;
}

.sidebar-nav {
	padding: 9px 0;
}
</style>

</head>
<!-- ********************************-->

<body ng-app="SPECS-APP-SLA-EDITOR">

	<div id="_navigator">
		<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"> <img alt="Brand"
					src="img/specs-logo-32.png"></a>
			</div>
			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li><a
						href="<%=request.getContextPath()%>/HomeDeveloper_Action.do">Home</a></li>
					<li><a
						href="<%=request.getContextPath()%>/BrowseMapping_Action.do">View
							Mapping</a></li>
					<li><a
						href="<%=request.getContextPath()%>/DeleteMapping_Action.do">Delete
							Mapping</a></li>

				</ul>
			</div>
			<!--/.nav-collapse -->
		</div>
		</nav>
	</div>

	<!-- Main jumbotron for a primary marketing message or call to action -->
	<div class="panel" ng-controller="slaEditContr">
		<div class="container">


			<div class="panel panel-default" ng-show="abstractMapping">
				<div class="panel-heading">
					<h1>Mapping details</h1>
				</div>

				<div class="panel panel-default">
					<div class="panel-body">
						<table class="table">
							<thead>
								<br>
								<h4><b>Id Mapping</b> : {{abstractMapping.idMapping}}
								</h4>
								<br />
								</br>

							</thead>
							<tbody>
								<tr>
									<td><div>Id Capability</div></td>
									<td><div>{{abstractMapping.capability}}</div></td>
								</tr>
								<tr>
									<td><div>Id Metric</div></td>
									<td><div>{{abstractMapping.metrica}}</div></td>
								</tr>
								<tr>
									<td><div>Controls</div></td>
									<td><div>{{abstractMapping.controllo}}</div></td>
								</tr>
							</tbody>
						</table>

					</div>
				</div>





			</div>
		</div>
	</div>


	<jsp:include page="../common/footer.jsp" />


	<script src="<%=request.getContextPath()%>/js/angular.js"></script>
	<!-- *************************** -->
	<script src="<%=request.getContextPath()%>/js/jquery.min.js"></script>
	<script src="<%=request.getContextPath()%>/js/jquery.xpath.js"></script>
	<script src="<%=request.getContextPath()%>/js/jquery.xml2json.js"></script>

	<script
		src="<%=request.getContextPath()%>/js/bootstrap_wizard/jquery.bootstrap.wizard.js"></script>

	<!-- Page constructors -->
	<!-- ANGULAR SCRIPT -->

	<script>
		var app = angular.module('SPECS-APP-SLA-EDITOR', []);

		app.controller('slaEditContr', [ '$scope', function($scope) {
			console.log("chiamata inizializzazione angular");
			$scope.metricUserName = "";
			
			$scope.xmlRetrieved = unescape(${pippo});
			console.log($scope.xmlRetrieved);
			
			
			$scope.capabilityTemplate = $.parseXML($scope.xmlRetrieved);
		    $scope.abstractMapping = $.xml2json($scope.capabilityTemplate);
		  
		  
		    console.log($scope.abstractMapping);
		    
		     
		    
		    
		    
		} ]);
	</script>

	<script>
	function findAndReplace(object, value, replacevalue){
		  for(var x in object){
		    if(typeof object[x] == 'object'){
		      findAndReplace(object[x], value, replacevalue);
		    }
		    if(object[x] == value){ 
		      object["name"] = replacevalue;
		      // break; // uncomment to stop after first replacement
		    }
		  }
		}
	</script>
</body>
</html>