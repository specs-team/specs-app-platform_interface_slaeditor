<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>SPECS SLA editor</title>

<link href="bootstrap/css/bootstrap.min.css" type="text/css"
	rel="stylesheet">
<style type="text/css">
body {
	padding-top: 60px;
	padding-bottom: 40px;
}

.sidebar-nav {
	padding: 9px 0;
}
</style>

</head>
<!-- ********************************-->

<body>
	<div id="_navigator">
		<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"> <img alt="Brand"
					src="img/specs-logo-32.png"></a>
			</div>
			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li><a
						href="<%=request.getContextPath()%>/HomeOwner_Action.do">Home</a></li>
					<li><a
						href="<%=request.getContextPath()%>/CreateSDT_Action.do">Create
							SLA template</a></li>
					<li><a
						href="<%=request.getContextPath()%>/StoreWsag_Action.do">Store
							SLA template</a></li>
				</ul>
			</div>
			<!--/.nav-collapse -->
		</div>
		</nav>
	</div>
	<div class="panel">
		<div class="container">

			<div class="panel panel-default">
				<div class="panel-heading">
					<h1>Create Service Description Term</h1>
					<h3>In this section you can create a new SLA Template. Please insert a new Service Description Term (SDT) section to the template, by specifying the available Cloud resources and the capability to be offered to End-users according to the following steps.</h3>
			


				<div class="panel panel-default">
					<div class="panel-body">
						<h2>
							1 - Add a Cloud resource section to the SDT
							</h2>
							<h4>Note: this application considers a default resource section.</h4>
							<a class="btn btn-primary btn-large"
								href="<%=request.getContextPath()%>/CreateResource_Action.do"
								target="_self">Add resource section</a>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-body">

						<h2>
							2 - Add capabilities section to the SDT
							</h2>
							<h4>Select capabilities to offer to End-users</h4>

							<html:form action="/CreateSDT_Action">
								<big><logic:iterate name="capabilityForm" id="capabilityForm">
									<html:multibox styleClass="optradio"
										property="selectedItems">
										<bean:write name="capabilityForm" property="value" />
									</html:multibox>
									<bean:write name="capabilityForm" property="label" /></big>
								</logic:iterate>
								<br />
								<br />
								<html:submit title="Invia" />
							</html:form>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-body">

						<h2>
							3 - Add a security metric section to the SDT
							</h2>
							<h4>Note that all security metrics associated with the selected capabilities will be included</h4>
							<a class="btn btn-primary btn-large"
								href="<%=request.getContextPath()%>/SelectSecurityMetric_Action.do"
								target="_self">Add security metrics</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>

	<jsp:include page="../common/footer.jsp" />

</body>
</html>