package logic;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import xml.JDOMWriter;
import entity.MappingCapMetric;
import entity.SecurityMetric;

public class AddSecurityMetric {
	private String[] capabilities;
	
	
	
	
	
	
	
	
	
	public AddSecurityMetric() throws ParserConfigurationException, SAXException, IOException{
		JDOMWriter xmlWriter=JDOMWriter.getInstance();
		this.capabilities=xmlWriter.getCapabilities();
		
		ArrayList<MappingCapMetric> arrayListMapping=new ArrayList<MappingCapMetric>();
		ArrayList<MappingCapMetric> arrayListMappingPulito=new ArrayList<MappingCapMetric>();
		
		MappingCapMetric mapping= new MappingCapMetric();
		arrayListMapping=mapping.catturaSecurityMetric(capabilities);

		ArrayList<SecurityMetric> arrayListMetric=new ArrayList<SecurityMetric>();
		SecurityMetric securityMetric=new SecurityMetric();
		arrayListMetric=securityMetric.catturaSecurityMetric();
		
		arrayListMappingPulito=pulisciMetriche(arrayListMapping);
		
		for(int i=0;i<arrayListMappingPulito.size();i++){
			for(int j=0;j<arrayListMetric.size();j++){
				if(arrayListMappingPulito.get(i).getMetrica().compareTo(arrayListMetric.get(j).getIdMetric())==0){
					ArrayList<SecurityMetric> arrayLsecurityMetric=new ArrayList<SecurityMetric>();
					arrayLsecurityMetric=parsingSecurityMetric(arrayListMetric.get(i).getMetricDescription());
					xmlWriter.salvaSecurityMetric(arrayLsecurityMetric);
				}
			}
		}
		
		
		System.out.println(xmlWriter.convertiXML());
	}
	
	
	public static ArrayList<MappingCapMetric> pulisciMetriche(ArrayList<MappingCapMetric> arrayListMapping ){
		ArrayList<MappingCapMetric> arrayListMappingPulito=new ArrayList<MappingCapMetric>();
		int n=arrayListMapping.size();
		int j=0;
		for(int i=0;i<n;i++){
			boolean esiste=false;
			for(int k=i+1;k<n;k++){
				if(arrayListMapping.get(i).getMetrica().compareTo(arrayListMapping.get(k).getMetrica())==0 && esiste==false){
					esiste=true;
				}
			}
			if(esiste==false){
				//arrayMetrica[j]=arrayMetrica[i];
				j++;
				arrayListMappingPulito.add(arrayListMapping.get(i));
				;
			}
		}
		return arrayListMappingPulito;
	}
	
	public static ArrayList<SecurityMetric> parsingSecurityMetric(String prova) throws ParserConfigurationException, SAXException, IOException{
		ArrayList<SecurityMetric> arrayLsecurityMetric=new ArrayList<SecurityMetric>();
		SecurityMetric sm=new SecurityMetric();
		DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		InputSource is = new InputSource();
		is.setCharacterStream(new StringReader(prova));
		Document doc=db.parse(is);
		Node root=doc.getFirstChild();
		//System.out.println(root.getNodeName());
		if(root.hasAttributes()){
			arrayLsecurityMetric.add(sm);
			arrayLsecurityMetric.get(0).setName(root.getAttributes().item(0).getNodeValue());
			arrayLsecurityMetric.get(0).setReferenceId(root.getAttributes().item(1).getNodeValue());
//			System.out.println(root.getAttributes().item(0).getNodeValue());
//			System.out.println(root.getAttributes().item(1).getNodeValue());
		}
			Node metricDefinition=root.getChildNodes().item(1);
			//System.out.println(metricDefinition.getNodeName());
			Node metricRules=root.getChildNodes().item(3);
			//System.out.println(metricRules.getNodeName());
			Node metricParameters=root.getChildNodes().item(5);
			//System.out.println(metricParameter.getNodeName());
			
			Node unit =metricDefinition.getChildNodes().item(1);
			if(unit.hasAttributes()){
				//System.out.println(unit.getAttributes().item(0).getNodeValue());
				arrayLsecurityMetric.get(0).setUnitName(unit.getAttributes().item(0).getNodeValue());
			}
			Node intervalUnit=unit.getChildNodes().item(3);
//			System.out.println(intervalUnit.getChildNodes().item(1).getTextContent());
//			System.out.println(intervalUnit.getChildNodes().item(3).getTextContent());
//			System.out.println(intervalUnit.getChildNodes().item(5).getTextContent());
//			System.out.println(intervalUnit.getChildNodes().item(7).getTextContent());
			arrayLsecurityMetric.get(0).setIntervalType(intervalUnit.getChildNodes().item(1).getTextContent());
			arrayLsecurityMetric.get(0).setIntervalStart(intervalUnit.getChildNodes().item(3).getTextContent());
			arrayLsecurityMetric.get(0).setIntervalStop(intervalUnit.getChildNodes().item(5).getTextContent());
			arrayLsecurityMetric.get(0).setIntervalSpep(intervalUnit.getChildNodes().item(7).getTextContent());
			
			
			Node scale =metricDefinition.getChildNodes().item(3);
			//System.out.println(scale.getChildNodes().item(1).getTextContent());
			arrayLsecurityMetric.get(0).setScale(scale.getChildNodes().item(1).getTextContent());
			Node expression =metricDefinition.getChildNodes().item(5);
			//System.out.println(expression.getTextContent());
			arrayLsecurityMetric.get(0).setEspression(expression.getTextContent());
			Node definition =metricDefinition.getChildNodes().item(7);
			//System.out.println(definition.getTextContent());
			arrayLsecurityMetric.get(0).setDefinition(definition.getTextContent());
			
			
			Node metricRule= metricRules.getChildNodes().item(1);
			if(metricRule.hasAttributes()){
				//System.out.println(metricRule.getAttributes().item(0).getNodeValue());
				arrayLsecurityMetric.get(0).setRuleId(metricRule.getAttributes().item(0).getNodeValue());
			}
			//System.out.println(metricRule.getChildNodes().item(1).getTextContent());
			arrayLsecurityMetric.get(0).setRuleDescription(metricRule.getChildNodes().item(1).getTextContent());
			//System.out.println(metricRule.getChildNodes().item(3).getTextContent());
			arrayLsecurityMetric.get(0).setRuleValue(metricRule.getChildNodes().item(3).getTextContent());;
			
			if(metricParameters.hasAttributes()){
				//System.out.println(metricParameters.getAttributes().item(0).getNodeValue());
				arrayLsecurityMetric.get(0).setParamId(metricParameters.getAttributes().item(0).getNodeValue());
			}
			//System.out.println(metricParameters.getChildNodes().item(1).getTextContent());
			arrayLsecurityMetric.get(0).setParamDescription(metricParameters.getChildNodes().item(1).getTextContent());
			//System.out.println(metricParameters.getChildNodes().item(3).getTextContent());
			arrayLsecurityMetric.get(0).setParamType(metricParameters.getChildNodes().item(3).getTextContent());
			//System.out.println(metricParameters.getChildNodes().item(5).getTextContent());
			arrayLsecurityMetric.get(0).setParamValue(metricParameters.getChildNodes().item(5).getTextContent());
			
		return arrayLsecurityMetric;
	}
	
	
	
	
	
	
	

}
