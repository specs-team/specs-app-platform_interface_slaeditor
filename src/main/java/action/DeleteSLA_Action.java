package action;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.LabelValueBean;

import db.DBWsag;
import entity.Wsag;
import form.DeleteSLA_Form;

public class DeleteSLA_Action extends Action{

	private Collection wsagForm =new ArrayList();
	private ArrayList<Wsag> arrayListWsag=new ArrayList<Wsag>();


	/**
	 * This method performs the override of the execute method of class Action and gives in output an ActionForward.
	 * It allow to login and save user in the session.   
	 *
	 * @param mapping        The ActionMapping.
	 * @param form        The ActionForm.
	 * @param request        The HttpServletRequest.
	 * @param response        The HttpServletResponse.      
	 * @return       The ActionForward
	 * @throws Exception the exception
	 */
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
					throws Exception {

		String wsagSelezionati=null;
		Wsag wsag=new Wsag();
		arrayListWsag=wsag.catturaWsag();

		wsagForm.clear();
		for(int i=0;i<arrayListWsag.size();i++){
			wsagForm.add(new LabelValueBean("Wsag "+arrayListWsag.get(i).getIdWsag() , arrayListWsag.get(i).getIdWsag()));
		}
		request.setAttribute("wsagForm", wsagForm);

		if(form!=null){
			DeleteSLA_Form deleteSLAForm=(DeleteSLA_Form)form;
			wsagSelezionati=deleteSLAForm.getSelectedItems();


			if(wsagSelezionati!=null){
				System.err.println(wsagSelezionati);
				DBWsag dbWsag=new DBWsag();
				dbWsag.eliminaWsag(wsagSelezionati);

			}

		}


		return mapping.findForward("success");
	}

}

