To use the Platform Interface application you must:

1 - Install a MySQL Database on your machine

2 - Create a Database with name ‘NIST’

3 - Execute the restore of all data into the database using the nist_all_dataabse.sql file (the file is located into the resources folder under src/main/java), to restore the .sql file you can use for example mysqldump command line tool executing the command: 'mysql -u <username> -p<password> NIST <  nist_all_dataabse.sql' where <username> and <password> can be 'root' 'root' according to the standard configuration of mysql

4 - Change the parameters of the database (dbName, userName, password) into the class DBConnectionManager.java under the package db with your


Into the nist_all_dataabse.sql database are registered three users:
- username: casola password: casola with owner permissions
- username: rak password: rak with developer permissions
- username: andrea password: andrea with user permissions
You can use these users in order to access into the application.