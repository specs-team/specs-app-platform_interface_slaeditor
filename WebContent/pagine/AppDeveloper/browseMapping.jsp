<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>SPECS SLA editor	</title>

<link href="bootstrap/css/bootstrap.min.css"  type="text/css" rel="stylesheet">
<style type="text/css">
	body {
		padding-top: 60px;
		padding-bottom: 40px;
	}
	
	.sidebar-nav {
		padding: 9px 0;
	}
</style>

</head>
<!-- ********************************-->

<body>

<div id="_navigator">
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"> <img alt="Brand" src="img/specs-logo-32.png"></a>
			</div>
			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li><a href="<%=request.getContextPath()%>/HomeDeveloper_Action.do">Home</a></li>
					<li><a href="<%=request.getContextPath()%>/BrowseMapping_Action.do" >View Mapping</a></li>
					<li><a href="<%=request.getContextPath()%>/DeleteMapping_Action.do" >Delete Mapping</a></li>
				
				</ul>
			</div>
			<!--/.nav-collapse -->
		</div>
	</nav>
</div>

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="panel">
      <div class="container">
        <h1>Navigate security mapping between security metric and capability (security controls)</h1>
        <h3>In this section you can visualize all created mapping and access their details.</h3>
        <br/>
        <h4>Select a mapping</h4> 
          <html:form action="/BrowseMapping_Action" >
	   			<logic:iterate name="mappingForm" id="qvalue">
	    			<big><html:radio styleClass="optradio" property="selectedItems" idName="qvalue" value="value"/>
	    			<bean:write name="qvalue" property="value" /></big>
	    			<br />
	    			</logic:iterate>
	    			<br />
				<html:submit title="Invia" />
			</html:form>
      </div>
    </div>

<jsp:include page="../common/footer.jsp"/>

</body>
</html>