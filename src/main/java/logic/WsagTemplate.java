package logic;

import xml.JDOMWriter;

public class WsagTemplate {
	
	private String [] spSelezionati={};
	private String [] gtSelezionati={};
	
	public WsagTemplate(){	
	}

	public WsagTemplate(String []spSelezionati, String []gtSelezionati){
		this.spSelezionati=spSelezionati;
		this.gtSelezionati=gtSelezionati;
	}
	
	public void salvaSP(String []spSelezionati){
		this.spSelezionati=spSelezionati;
	}
	public void salvaGT(String []gtSelezionati){
		this.gtSelezionati=gtSelezionati;
	}
	
	public void addServiceProprieties(){
		JDOMWriter xmlWriter=JDOMWriter.getInstance();
		for(int i=0;i<spSelezionati.length;i++){
			if(spSelezionati[i].compareTo("SPWebPool")==0)
				xmlWriter.creaServiceProperties_WebPool();
			if(spSelezionati[i].compareTo("SPSva")==0)
				xmlWriter.creaServiceProperties_Sva();
			if(spSelezionati[i].compareTo("SPOpenVas")==0)
				xmlWriter.creaServiceProperties_OpenVas();
			if(spSelezionati[i].compareTo("SPOssec")==0)
				xmlWriter.creaServiceProperties_Ossec();
		}
		
	}
	public void addGuaranteeTerms(){
		JDOMWriter xmlWriter=JDOMWriter.getInstance();
		for(int i=0;i<gtSelezionati.length;i++){
			if(gtSelezionati[i].compareTo("GTWebPool")==0)
				xmlWriter.creaGT_WebPool();
			if(gtSelezionati[i].compareTo("GTSva")==0)
				xmlWriter.creaGT_Sva();
			if(gtSelezionati[i].compareTo("GTOpenVas")==0)
				xmlWriter.creaGT_OpenVas();
			if(gtSelezionati[i].compareTo("GTOssec")==0)
				xmlWriter.creaGT_Ossec();
		}
		
	}
		
}

