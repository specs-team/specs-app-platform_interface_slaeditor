<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<div id="_navigator">
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"> <img alt="Brand"
					src="img/specs-logo-32.png"></a>
			</div>
			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li><a href="<%=request.getContextPath()%>/HomeDeveloper_Action.do" >Home</a></li>
					<li><a href="<%=request.getContextPath()%>/CreateSC_Action.do" >Create Capability</a></li>
					<li><a href="<%=request.getContextPath()%>/AssociateSM_Action.do" >Associate Security Metric</a></li>
					<li><a href="<%=request.getContextPath()%>/BrowseCapability_Action.do">Browse & View Capabilities</a></li>
					<li><a href="<%=request.getContextPath()%>/BrowseSecurityMetric_Action.do">Browse & View Security Metrics</a></li>
					<li><a href="<%=request.getContextPath()%>/BrowseMapping_Action.do">Browse & View Mapping Metrics</a></li>
					
				</ul>
				<a class="navbar-brand pull-right"href="<%=request.getContextPath()%>/Login_Action.do">New Log In</a>
			</div>
		</div>
	</nav>
</div>
