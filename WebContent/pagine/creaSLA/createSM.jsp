<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>TESI APP	</title>

<link href="bootstrap/css/bootstrap.min.css"  type="text/css" rel="stylesheet">
<style type="text/css">
	body {
		padding-top: 60px;
		padding-bottom: 40px;
	}
	
	.sidebar-nav {
		padding: 9px 0;
	}
</style>

</head>
<!-- ********************************-->

<body>

<jsp:include page="../common/navigatorDeveloper.jsp">
<jsp:param  name="_home" value="class='active'"/>
</jsp:include>

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="panel">
      <div class="container">
        <h1>Associate one Security Metric with a Security Capability</h1>
        <p>In this section, you can associate a Security Metric to a Capability, and in particular to the security controls the Capability is composed of.
        </br> Please follow the next steps:</p>
          <h2>1 - Select a Security Metric</h2>
          <html:form action="/AssociateSM_Action" >
	   			<logic:iterate name="smForm" id="qvalue">
	    			<html:radio styleClass="form-control" property="selectedItems" idName="qvalue" value="value"/>
	    			<bean:write name="qvalue" property="value" />
	    			<br />
	    			</logic:iterate>
				<html:submit title="Invia" />
			</html:form>
          
 		<h2>2 - Select Capability Metric</h2>
          <html:form action="/AssociateSM_Action" >
	   			<logic:iterate name="capabilityForm" id="qvalue">
	    			<html:radio styleClass="form-control" property="selectedItems2" idName="qvalue" value="value"/>
	    			<bean:write name="qvalue" property="value" />
	    			<br />
	    			</logic:iterate>
				<html:submit title="Invia" />
			</html:form>          
          
      </div>
    </div>

<jsp:include page="../common/footer.jsp"/>

</body>
</html>