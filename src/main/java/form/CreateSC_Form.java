package form;

import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

public class CreateSC_Form extends ActionForm {
	
	private String nome=null;
	
	public void setNome(String nome){
		this.nome=nome;
	}
	public String getNome(){
		return this.nome;
	}
	
	private String[] selectedItems ={};
	
	
	public String[] getSelectedItems(){
		return selectedItems;
	}
	
	public void setSelectedItems(String[] selectedItems){
		this.selectedItems=selectedItems;
	}
	
	private String[] selectedItems2={};
	
	public String[] getSelectedItems2(){
		return selectedItems2;
	}
	
	public void setSelectedItems2(String[] selectedItems2){
		this.selectedItems2=selectedItems2;
	}
	

}