<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>SPECS SLA editor	</title>

<link href="bootstrap/css/bootstrap.min.css"  type="text/css" rel="stylesheet">
<style type="text/css">
	body {
		padding-top: 60px;
		padding-bottom: 40px;
	}
	
	.sidebar-nav {
		padding: 9px 0;
	}
</style>

</head>
<!-- ********************************-->

<body>

<jsp:include page="../common/navigatorDeveloper.jsp">
<jsp:param  name="_home" value="class='active'"/>
</jsp:include>

    <!-- Main jumbotron for a primary marketing message or call to action -->
     <div class="panel">
      <div class="container">
        <h1>Please associate the security metric with specific security controls of the selected capability </h1>
          <h4>Select security controls</h4>
           	<html:form action="/AssociateSM_Action">
				<logic:iterate name="controlli" id="controlli">
					<big><html:multibox property="selectedItems3">
						<bean:write name="controlli" property="value" />
					</html:multibox>
					<bean:write name="controlli" property="label" /></big>
					<br/>
				</logic:iterate>
				<br/>
				<html:submit title="Invia" />
			</html:form>       
          
      </div>
    </div>

<jsp:include page="../common/footer.jsp"/>

</body>
</html>