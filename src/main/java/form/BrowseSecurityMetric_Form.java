package form;

import org.apache.struts.action.ActionForm;

public class BrowseSecurityMetric_Form extends ActionForm{
	
	private String selectedItems=null;
	
	
	public String getSelectedItems(){
		return this.selectedItems;
	}
	
	public void setSelectedItems(String selectedItems){
		this.selectedItems=selectedItems;
	}

}
