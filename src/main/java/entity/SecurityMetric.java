package entity;

import java.util.ArrayList;

import db.DBSecurityMetric;


public class SecurityMetric {
	
	private String idMetric;
	private String metricDescription;
	
	private String name;
	private String referenceId;
	private String unitName;
	private String intervalType;
	private String intervalStart;
	private String intervalStop;
	private String intervalSpep;
	private String scale;
	private String espression;
	private String definition;
	
	private String ruleId;
	private String ruleDescription;
	private String ruleValue;
	
	private String paramId;
	private String paramDescription;
	private String paramType;
	private String paramValue;
	
	public SecurityMetric(){	
	}
	
	
	public SecurityMetric(String idMetric, String metricDescription ){
		this.idMetric=idMetric;
		this.metricDescription=metricDescription;
		DBSecurityMetric dbSecurityMetric= new DBSecurityMetric(idMetric,metricDescription);
	}
	
	public ArrayList<SecurityMetric> catturaSecurityMetric(){
		int count=0;
		int campi=2;
		DBSecurityMetric dbSM=new DBSecurityMetric();
		ArrayList<SecurityMetric> arrayListWsag= new ArrayList<SecurityMetric>();
		ArrayList arrayList= new ArrayList();
		arrayList=dbSM.catturaSecurityMetric();
		for(int i=0;i<arrayList.size();i=i+campi){
			SecurityMetric sm=new SecurityMetric();
			arrayListWsag.add(sm);
			arrayListWsag.get(count).idMetric=(String)arrayList.get(i);
			arrayListWsag.get(count).metricDescription=(String)arrayList.get(i+1);
			count++;
		}
		return arrayListWsag;
	}
	
	public String getIdMetric(){
		return this.idMetric;
	}
	
	public String getMetricDescription(){
		return this.metricDescription;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getReferenceId() {
		return referenceId;
	}


	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}


	public String getUnitName() {
		return unitName;
	}


	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}


	public String getIntervalType() {
		return intervalType;
	}


	public void setIntervalType(String intervalType) {
		this.intervalType = intervalType;
	}


	public String getIntervalStart() {
		return intervalStart;
	}


	public void setIntervalStart(String intervalStart) {
		this.intervalStart = intervalStart;
	}


	public String getIntervalStop() {
		return intervalStop;
	}


	public void setIntervalStop(String intervalStop) {
		this.intervalStop = intervalStop;
	}


	public String getIntervalSpep() {
		return intervalSpep;
	}


	public void setIntervalSpep(String intervalSpep) {
		this.intervalSpep = intervalSpep;
	}


	public String getScale() {
		return scale;
	}


	public void setScale(String scale) {
		this.scale = scale;
	}


	public String getEspression() {
		return espression;
	}


	public void setEspression(String espression) {
		this.espression = espression;
	}


	public String getDefinition() {
		return definition;
	}


	public void setDefinition(String definition) {
		this.definition = definition;
	}


	public String getRuleId() {
		return ruleId;
	}


	public void setRuleId(String ruleId) {
		this.ruleId = ruleId;
	}


	public String getRuleDescription() {
		return ruleDescription;
	}


	public void setRuleDescription(String ruleDescription) {
		this.ruleDescription = ruleDescription;
	}


	public String getRuleValue() {
		return ruleValue;
	}


	public void setRuleValue(String ruleValue) {
		this.ruleValue = ruleValue;
	}


	public String getParamId() {
		return paramId;
	}


	public void setParamId(String paramId) {
		this.paramId = paramId;
	}


	public String getParamDescription() {
		return paramDescription;
	}


	public void setParamDescription(String paramDescription) {
		this.paramDescription = paramDescription;
	}


	public String getParamType() {
		return paramType;
	}


	public void setParamType(String paramType) {
		this.paramType = paramType;
	}


	public String getParamValue() {
		return paramValue;
	}


	public void setParamValue(String paramValue) {
		this.paramValue = paramValue;
	}
	
	
}
