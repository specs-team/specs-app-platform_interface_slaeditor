package action;
import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import logic.AssociateMetricControl;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.LabelValueBean;

import xml.CapabilityXML;
import xml.JDOMWriter;
import entity.Capability;
import entity.Controllo;
import entity.MappingCapMetric;
import entity.SecurityMetric;
import form.AssociateSM_Form;




public class AssociateSM_Action extends Action {
	
	public Collection smForm=new ArrayList();
	public Collection capabilityForm=new ArrayList();
	public Collection controlli=new ArrayList();
	public ArrayList<SecurityMetric> arrayListSecurityMetric=new ArrayList<SecurityMetric>();
	public ArrayList<Capability> arrayListCapability= new ArrayList<Capability>();
	public ArrayList<Capability> arrayListCapabilityPulite= new ArrayList<Capability>();
	public ArrayList<Controllo>  arrayListControllo=new ArrayList<Controllo>();
	public ArrayList<Controllo>  arrayListControllopuliti=new ArrayList<Controllo>();
	private String controlliXML=null;
	
	/**
	 * This method performs the override of the execute method of class Action and gives in output an ActionForward.
	 * It allow to login and save user in the session.   
	 *
	 * @param mapping        The ActionMapping.
	 * @param form        The ActionForm.
	 * @param request        The HttpServletRequest.
	 * @param response        The HttpServletResponse.      
	 * @return       The ActionForward
	 * @throws Exception the exception
	 */
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		System.out.println("Sono nella classe AssociateSM_Action");
		String returnMapping="";
		
		SecurityMetric sm=new SecurityMetric();
		arrayListSecurityMetric=sm.catturaSecurityMetric();
		
		Capability capability=new Capability();
		arrayListCapability=capability.catturaCapability();
		String smSelezionati;
		String capabilitySelezionata;
		String [] controlliSelezionati=null;
		String nome=null;
		
		
		smForm.clear();
		for(int i=0;i<arrayListSecurityMetric.size();i++){
			smForm.add((new LabelValueBean(arrayListSecurityMetric.get(i).getIdMetric(),arrayListSecurityMetric.get(i).getIdMetric())));
		}
		request.setAttribute("smForm", smForm);
		
		capabilityForm.clear();
		for(int i=0;i<arrayListCapability.size();i++){
			capabilityForm.add(new LabelValueBean("Capability "+arrayListCapability.get(i).getIdCapability() , arrayListCapability.get(i).getIdCapability()));
		}
		request.setAttribute("capabilityForm", capabilityForm);
		
		returnMapping="success";
		if(form!=null){
			AssociateSM_Form associateSMform=(AssociateSM_Form)form;
			smSelezionati=associateSMform.getSelectedItems();
			capabilitySelezionata=associateSMform.getSelectedItems2();
			controlliSelezionati=associateSMform.getSelectedItems3();
			nome=associateSMform.getNome();
			if(smSelezionati!=null){
				smForm.clear();
				System.out.println(smSelezionati);
				returnMapping="success";
				AssociateMetricControl associateMetricControl=AssociateMetricControl.getInstance();
				associateMetricControl.setSecurityMetric(smSelezionati);
			}
			
			if(capabilitySelezionata!=null){
//				System.out.println(capabilitySelezionata);
				arrayListCapabilityPulite=pulisciCapability(capabilitySelezionata,arrayListCapability);
				controlliXML=stampaArrayList(arrayListCapabilityPulite);
//				System.err.println(controlliXML);
				CapabilityXML capabilitiXML=new CapabilityXML();
				arrayListControllo=capabilitiXML.estraiControlli(controlliXML);
				
				controlli.clear();
				for(int i=0;i<arrayListControllo.size();i++){
					controlli.add(new LabelValueBean(arrayListControllo.get(i).getIdControl() , arrayListControllo.get(i).getIdControl()));
				}
				request.setAttribute("controlli", controlli);
				returnMapping="update";
				AssociateMetricControl associateMetricControl=AssociateMetricControl.getInstance();
				associateMetricControl.setCapability(capabilitySelezionata);
				
			}
			if(controlliSelezionati.length!=0){
//				for(int i=0;i<controlliSelezionati.length;i++){
//					System.out.println(controlliSelezionati[i]);
//				}
				AssociateMetricControl associateMetricControl=AssociateMetricControl.getInstance();
				associateMetricControl.setControlli(controlliSelezionati);
//				System.out.println("Stampo security metric "+associateMetricControl.getSecurityMetric());
//				System.out.println("Stampo la capability  "+associateMetricControl.getCapability());
				returnMapping="finish";
				//MappingCapMetric mappingCM=new MappingCapMetric(associateMetricControl.getCapability(),associateMetricControl.getControlli(),associateMetricControl.getSecurityMetric());
				
			}
			
			if(nome!=null){
				System.out.println(nome);
				AssociateMetricControl associateMetricControl=AssociateMetricControl.getInstance();
				
				MappingCapMetric mappingCM=new MappingCapMetric(nome,associateMetricControl.getCapability(),associateMetricControl.getControlli(),associateMetricControl.getSecurityMetric());
				
//				System.out.println("Stampo security metric "+associateMetricControl.getSecurityMetric());
//				System.out.println("Stampo la capability  "+associateMetricControl.getCapability());
				returnMapping="back";
			}
			
			
		}	
			
		
		
		return mapping.findForward(returnMapping);
	}
	
	
	
	
	
	
	
	public ArrayList<Capability> pulisciCapability(String capabilitySelezionate, ArrayList<Capability> arrayListCapability){
		ArrayList<Capability> arrayListCapabilityPulite = new ArrayList<Capability>();
		int k=0;
		for(int i=0;i<arrayListCapability.size();i++){
				if(capabilitySelezionate.compareTo(arrayListCapability.get(i).getIdCapability())==0){
					//System.out.println("sono uguali");
					arrayListCapabilityPulite.add(arrayListCapability.get(i));
					k++;
				}
		}
	return arrayListCapabilityPulite;
	}
	
	
	public String stampaArrayList(ArrayList<Capability> arrayListCapabilityPulite){
		//System.err.println(arrayListCapabilityPulite.size());
		String prova=null;
		for(int i=0;i<arrayListCapabilityPulite.size();i++){
			//System.out.println(arrayListCapabilityPulite.get(i).getIdCapability());
			prova=arrayListCapabilityPulite.get(i).getCapabilityDescription();
			
		}//System.out.println(prova);
		return prova;
	}

}