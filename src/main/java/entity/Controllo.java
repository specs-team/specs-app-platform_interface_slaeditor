package entity;

import java.util.ArrayList;

import db.*;

public class Controllo {
	
	private String idControl;
	private String controlName;
	private String familyId;
	private String familyName;
	private int control;
	private int enhancement;
	private String description;
	
	public Controllo(){
	}
	
	public Controllo(String familyId){
//		DBControllo dbControllo= new DBControllo(familyId);
//		this.idControl=dbControllo.getIdControl();
//		this.controlName=dbControllo.getControlName();
//		this.familyId=dbControllo.getFamilyId();
//		this.familyName=dbControllo.getFamilyName();
//		this.control=dbControllo.getControl();
//		this.enhancement=dbControllo.getEnhancement();
	}
	
	public Controllo(String idControl, String controlName, String familyId, 
			String familyName, int control, int enhancement, String description){
		this.idControl=idControl;
		this.controlName=controlName;
		this.familyId=familyId;
		this.familyName=familyName;
		this.control=control;
		this.enhancement=enhancement;
		this.description=description;
//		System.out.println("Stampo id "+idControl);
//		System.out.println("Stampo name "+controlName);
//		System.out.println("Stampo familyId "+familyId);
//		System.out.println("Stampo familyName "+familyName);
//		System.out.println("Stampo control "+control);
//		System.out.println("Stampo enhancement "+enhancement);
//		System.out.println("Stampo la descrizione "+description);
		DBControllo dbControllo = new DBControllo(idControl,controlName,familyId,familyName,control,enhancement,description);
		}
	
	
	public ArrayList<Controllo> catturaControlli(String[] familyId){
		int count =0;
		int campi =7;
		DBControllo dbControllo=new DBControllo();
		ArrayList<Controllo> arrayListControllo = new ArrayList<Controllo>(); //arrayList che mi ritorna che in ogni locazione ho un controllo
		ArrayList arrayList=new ArrayList();								//arrayList generico che mi ritorna dalla funzione dbControllo.caturaControlli
		
		arrayList=dbControllo.catturaControlli(familyId);
		for(int i=0;i<arrayList.size();i=i+campi){
			Controllo controllo = new Controllo();
			arrayListControllo.add(controllo);
			arrayListControllo.get(count).idControl=(String)arrayList.get(i);
			arrayListControllo.get(count).controlName=(String)arrayList.get(i+1);
			arrayListControllo.get(count).familyId=(String)arrayList.get(i+2);
			arrayListControllo.get(count).familyName=(String)arrayList.get(i+3);
			arrayListControllo.get(count).control=Integer.parseInt((String)arrayList.get(i+4));
			arrayListControllo.get(count).enhancement=Integer.parseInt((String)arrayList.get(i+5));
			arrayListControllo.get(count).description=(String)arrayList.get(i+6);
			count++;
		}
		return arrayListControllo;
	}
	
	
	public String getIdControl(){
		return this.idControl;
	}
	public String getControlName(){
		return this.controlName;
	}
	public String getFamilyId(){
		return this.familyId;
	}
	public String getFamilyName(){
		return this.familyName;
	}
	public int getControl(){
		return this.control;
	}
	public int getEnhancement(){
		return this.enhancement;
	}
	
	public String getDescription(){
		return this.description;
	}
	
	public void setIdControl(String idControl ){
		this.idControl=idControl;
	}
	public void setControlName(String controlName){
		this.controlName=controlName;
	}
	public void setFamilyId(String familyId){
		this.familyId=familyId;
	}
	public void setFamilyName(String familyName){
		this.familyName=familyName;
	}
	public void setControl(int control){
		this.control=control;
	}
	public void setEnhancement(int enhancement ){
		this.enhancement=enhancement;
	}
	public void setDescription(String description){
		this.description=description;
	}
		
}