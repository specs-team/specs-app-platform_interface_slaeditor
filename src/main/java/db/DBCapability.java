package db;

import java.sql.ResultSet;
import java.util.ArrayList;

import metadata.DB;

public class DBCapability {
	
	String idCpability;
	String capabilityDescription;
	
	
	
	
	//costruttore di default
	public DBCapability(){
		
	}
	
	//elimina capability
	public void eliminaCapability(String capabilitySelezionate){
		String query="DELETE FROM "+DB.DBName+"."+DB.capability+" WHERE idCapability='"+capabilitySelezionate+"';";
		try{
			DBConnectionManager.updateQuery(query);
		}catch(Exception e){
			e.printStackTrace();
		}
	}


	//scrivo le nuove capability
	public DBCapability(String idCapability, String capabilityDescription){
		this.idCpability=idCapability;
		this.capabilityDescription=capabilityDescription;
		
		String query="INSERT INTO "+DB.DBName+"."+DB.capability+"(idCapability,capabilityDescription) VALUES('"+idCapability+"','"+capabilityDescription+"');";
		try{
			DBConnectionManager.updateQuery(query);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	//Catturo tutte la capability che sono sul DB
	public ArrayList catturaCapability(){
		ArrayList arrayList= new ArrayList();
		String query="SELECT * FROM "+DB.DBName+"."+DB.capability;
		try {			
			ResultSet rs = DBConnectionManager.selectQuery(query);
			while(rs.next()) {
				arrayList.add(rs.getString("idCapability"));
				arrayList.add(rs.getString("capabilityDescription"));
			}
		} catch(Exception e){
			e.printStackTrace();
		}
		return arrayList;
	}
	
	
}
