package db;

import java.sql.ResultSet;
import java.util.ArrayList;

import metadata.DB;

public class DBMappingCapMetric {
	
	private String idMapping;
	private String capability;
	private String metrica;
	private String controllo;
	private String mappingDescription;
	
	
	public DBMappingCapMetric(){
		
	}
	
	//elimina mapping
		public void eliminaMapping(String idMapping){
			String query="DELETE FROM "+DB.DBName+"."+DB.mappingmetric+" WHERE idMapping='"+idMapping+"';";
			try{
				DBConnectionManager.updateQuery(query);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	
	
	
	
	public DBMappingCapMetric(String idMapping,String capability,String controllo, String metrica,String mappingDescription){
		this.idMapping=idMapping;
		this.capability=capability;
		this.controllo=controllo;
		this.metrica=metrica;
		this.mappingDescription=mappingDescription;
		System.err.println(idMapping);
		String query="INSERT INTO "+DB.DBName+"."+DB.mappingmetric+"(capability,securityControl,securityMetric,idMapping,mappingDescription) VALUES('"+capability+"','"+controllo+"','"+metrica+"','"+idMapping+"','"+mappingDescription+"');";
		try{
			DBConnectionManager.updateQuery(query);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
//		for(int i=0;i<controlli.length;i++){
//			String query="INSERT INTO "+DB.DBName+"."+DB.mappingmetric+"(capability,securityControl,securityMetric) VALUES('"+capability+"','"+controlli[i]+"','"+metrica+"');";
//			try{
//				DBConnectionManager.updateQuery(query);
//			}catch(Exception e){
//				e.printStackTrace();
//			}
//		}
	}
	
	//Catturo tutti i mapping con tutte le info associate 
		public ArrayList catturaAllMapping(){
			ArrayList arrayList= new ArrayList();
			String query="SELECT * FROM "+DB.DBName+"."+DB.mappingmetric;
			try {			
				ResultSet rs = DBConnectionManager.selectQuery(query);
				while(rs.next()) {
					arrayList.add(rs.getString("capability"));
					arrayList.add(rs.getString("securityControl"));
					arrayList.add(rs.getString("securityMetric"));
					arrayList.add(rs.getString("idMapping"));
					arrayList.add(rs.getString("mappingDescription"));
				}
			} catch(Exception e){
				e.printStackTrace();
			}
			return arrayList;
		}
		
	
	
	
	
	
	public ArrayList catturaSecurityMetric(String[] capability){
		ArrayList arrayList= new ArrayList();
		boolean fine=false;
		String query="SELECT * FROM "+DB.DBName+"."+DB.mappingmetric+" where capability=";
		for(int i=0;i<capability.length;i++){
			query=query+"'"+capability[i]+"'";
			if(i==capability.length-1){
				fine=true;
			}
			System.out.println(fine);
			if(!fine){
				query=query+" OR capability=";
			}
		}
			try {			
				ResultSet rs = DBConnectionManager.selectQuery(query);
				while(rs.next()) {
					arrayList.add(rs.getString("capability"));
					arrayList.add(rs.getString("securityControl"));
					arrayList.add(rs.getString("securityMetric"));
				}
			} catch(Exception e){
				e.printStackTrace();
			}
		return arrayList;
	}
	
	public ArrayList catturaSecurityMetricSingolaCapability(String capability){
		ArrayList arrayList= new ArrayList();
		String query="SELECT * FROM "+DB.DBName+"."+DB.mappingmetric+" where capability='"+capability+"'";
		try {			
			ResultSet rs = DBConnectionManager.selectQuery(query);
			while(rs.next()) {
				arrayList.add(rs.getString("capability"));
				arrayList.add(rs.getString("securityControl"));
				arrayList.add(rs.getString("securityMetric"));
			}
			} catch(Exception e){
				e.printStackTrace();
		}
		return arrayList;
	}
	
	
}
	
