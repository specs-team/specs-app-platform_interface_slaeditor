package db;

import java.sql.ResultSet;
import java.util.ArrayList;

import metadata.DB;

public class DBSecurityMetric {
	
	private String idMetric;
	private String metricDescription;
	
	
	
	public DBSecurityMetric(){
		
		
	}
	
	public DBSecurityMetric(String idMetric, String metricDescription){
		this.idMetric=idMetric;
		this.metricDescription=metricDescription;
		String query="INSERT INTO "+DB.DBName+"."+DB.securitymetric+"(idMetric,metricDescription) VALUES('"+idMetric+"','"+metricDescription+"');";
		try{
			DBConnectionManager.updateQuery(query);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public ArrayList catturaSecurityMetric(){
		ArrayList arrayList= new ArrayList();
		String query="SELECT * FROM "+DB.DBName+"."+DB.securitymetric;
		try {			
			ResultSet rs = DBConnectionManager.selectQuery(query);
			while(rs.next()) {
				arrayList.add(rs.getString("idMetric"));
				arrayList.add(rs.getString("metricDescription"));
			}
		} catch(Exception e){
			e.printStackTrace();
		}
		return arrayList;
	}
	
	
}
