package xml;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.Namespace;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import entity.Controllo;

public class XmlCapability {
	
	private static XmlCapability istanza;
	private Namespace nsWsag,nsSpecs,nsNist,nsXs,nsXsi;
	private Document document;
	private Element root;
	private Element capability;
	private ArrayList<Controllo> arrayListControllo;
	
	//ritorna l'istanza
		public static XmlCapability getInstance(){
			if(istanza==null){
				istanza=new XmlCapability();
			}
			return istanza;
		}
	public XmlCapability(){
		
	}
	
	public void addCapabilityDescr(String id, String name, String description){
		capability.setAttribute("id", id);
		capability.setAttribute("name", name);
		capability.setAttribute("description", description);
		
	}
	
	public void XmlCapability(ArrayList<Controllo> arrayListControllo){
		this.arrayListControllo=arrayListControllo;
			
		nsSpecs=Namespace.getNamespace("specs", "http://specs-project.eu/schemas/sla_related");
		nsNist=Namespace.getNamespace("nist", "http://specs-project.eu/schemas/nist_control_framework");
		
		capability = new Element("capabilities",nsSpecs);
		capability.addNamespaceDeclaration(nsSpecs);
		capability.addNamespaceDeclaration(nsNist);
		document = new Document(capability);

		for(int i=0; i<arrayListControllo.size();i++){
			Element item1= new Element("security_control",nsSpecs);
			item1.setAttribute("id", arrayListControllo.get(i).getIdControl());
			item1.setAttribute("name",arrayListControllo.get(i).getControlName());
			item1.setAttribute("control_family_id",arrayListControllo.get(i).getFamilyId(),nsNist);
			item1.setAttribute("control_family_name",arrayListControllo.get(i).getFamilyName(),nsNist);
			String stringa1=Integer.toString(arrayListControllo.get(i).getControl());
			String stringa2=Integer.toString(arrayListControllo.get(i).getEnhancement());
			item1.setAttribute("security_control", stringa1,nsNist);
			item1.setAttribute("control_enhancement", stringa2,nsNist);
			Element descr1 = new Element("control_description",nsSpecs);
			descr1.setText(arrayListControllo.get(i).getDescription());
			item1.addContent(descr1);
			capability.addContent(item1);
			}
		}
		

	
	public String convertiXmlToString(){
		String stringXML;
		XMLOutputter outputter = new XMLOutputter();
		outputter.setFormat(Format.getPrettyFormat());
		stringXML=outputter.outputString(document);
		return stringXML;
		
	}

}
