<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>SPECS SLA editor	</title>

<link href="bootstrap/css/bootstrap.min.css"  type="text/css" rel="stylesheet">
<style type="text/css">
	body {
		padding-top: 60px;
		padding-bottom: 40px;
	}
	
	.sidebar-nav {
		padding: 9px 0;
	}
</style>

</head>
<!-- ********************************-->

<body>
<div id="_navigator">
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"> <img alt="Brand" src="<%=request.getContextPath()%>/img/specs-logo-32.png"></a>
			</div>
			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
				</ul>
				<a class="navbar-brand pull-right" href="<%=request.getContextPath()%>/Login_Action.do">Log In</a>
				
			</div>
			<!--/.nav-collapse -->
		</div>
	</nav>
</div>

	<div class="panel">
		<div class="container">
			<h1>Login page to the SPECS SLA-Editor application</h1>
			<h2>Please enter your credentials and select your role</h2>
			
			<html:form action="/Login_Action"  >
			<br/>
			Username : <html:text property="nome" /> <br/><br/>
			Password : <html:password property="pass" /><br/><br/> 
			Developer	<html:radio property="selectedItems" value="developer" />
			Owner 		<html:radio property="selectedItems" value="owner" />
			<br/><br/>
			<html:submit value="Login" />
			<br/>
			</html:form>
			<br/>
			<br/>

		
		
		</div>
	</div>


	<jsp:include page="common/footer.jsp"/>

</body>
</html>