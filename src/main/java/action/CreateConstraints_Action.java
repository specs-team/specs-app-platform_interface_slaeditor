package action;

import java.util.ArrayList;
import java.util.Collection;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.LabelValueBean;

import form.*;

public class CreateConstraints_Action extends Action {
	/**
	 * This method performs the override of the execute method of class Action and gives in output an ActionForward.
	 * It allow to login and save user in the session.   
	 *
	 * @param mapping        The ActionMapping.
	 * @param form        The ActionForm.
	 * @param request        The HttpServletRequest.
	 * @param response        The HttpServletResponse.      
	 * @return       The ActionForward
	 * @throws Exception the exception
	 */
	
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		String[] output = null;
		
		if(form!=null){
			//effettuo il cast del form in ingresso
			CreateConstraints_Form createConstraintsForm = (CreateConstraints_Form)form;
			output=createConstraintsForm.getSelectedItems();
			int i=0;
		    while(i<output.length){
		    	System.out.println(output[i]);
		    	i++;
		    }
		    
		}
		 
		
		//initial the customers collection
		Collection customers = new ArrayList();
		
		customers.add(new LabelValueBean("Marie", "1"));
		customers.add(new LabelValueBean("Klaus", "2"));
		customers.add(new LabelValueBean("Peter", "3"));
		//set customer collection in the request
		request.setAttribute("customers", customers);

		
		return mapping.findForward("success");
	}

}