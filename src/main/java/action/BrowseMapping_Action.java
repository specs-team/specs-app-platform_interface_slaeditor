package action;

import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.LabelValueBean;



import entity.MappingCapMetric;
import form.BrowseMapping_Form;


public class BrowseMapping_Action  extends Action{
	
	public Collection mappingForm=new ArrayList();
	public ArrayList<MappingCapMetric> arrayListMapping= new ArrayList<MappingCapMetric>();
	public ArrayList<MappingCapMetric> arrayListMappingpulito= new ArrayList<MappingCapMetric>();
	
	
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
	String returnMapping="";
	String  mappingSelezionato=null;
	MappingCapMetric mapp=new MappingCapMetric();
	arrayListMapping=mapp.catturaAllMapping();
	
	
	
//	SecurityMetric securityMetric=new SecurityMetric();
//	arrayListSM=securityMetric.catturaSecurityMetric();
//	
	mappingForm.clear();
	for(int i=0;i<arrayListMapping.size();i++){
		mappingForm.add(new LabelValueBean("Id Mapping "+arrayListMapping.get(i).getIdMapping() , arrayListMapping.get(i).getIdMapping()));
		}
	 
	request.setAttribute("mappingForm", mappingForm);
	returnMapping="success";
	
	
	if(form!=null){
		BrowseMapping_Form browseMapping=(BrowseMapping_Form)form;
		mappingSelezionato=browseMapping.getSelectedItems();
	    System.err.println(mappingSelezionato);
	
	
 	    String prova=null;
	    if(mappingSelezionato!=null){
	    	for(int i=0;i<arrayListMapping.size();i++){
	    			if(mappingSelezionato.compareTo(arrayListMapping.get(i).getIdMapping())==0)
	    					prova=arrayListMapping.get(i).getMappingDescription();
	    		}
	    	}

	    if(prova!=null){
		System.err.println(prova);
		request.setAttribute("pippo","\""+StringEscapeUtils.escapeJava(prova)+"\"");
		returnMapping="update";
	}
		
	}
	
	

	
 
		return mapping.findForward(returnMapping);
	}
	
	
	

}
 
