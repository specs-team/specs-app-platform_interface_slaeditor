package entity;
import java.util.ArrayList;

import db.DBCapability;
import db.DBWsag;
public class Wsag {
	private String idWsag;
	private String wsagDescription;
	
	
	public Wsag(){
	}
	
	public Wsag(String idWsag, String wsagDescription){
		this.idWsag=idWsag;
		this.wsagDescription=wsagDescription;
		DBWsag dbWsag= new DBWsag(idWsag,wsagDescription);
	}
	
	public ArrayList<Wsag> catturaWsag(){
		int count=0;
		int campi=2;
		DBWsag dbWsag=new DBWsag();
		ArrayList<Wsag> arrayListWsag= new ArrayList<Wsag>();
		ArrayList arrayList= new ArrayList();
		arrayList=dbWsag.catturaWsag();
		for(int i=0;i<arrayList.size();i=i+campi){
			Wsag wsag=new Wsag();
			arrayListWsag.add(wsag);
			arrayListWsag.get(count).idWsag=(String)arrayList.get(i);
			arrayListWsag.get(count).wsagDescription=(String)arrayList.get(i+1);
			count++;
		}
		return arrayListWsag;
	}
	
	public String getIdWsag(){
		return this.idWsag;
	}
	public void setIdWsag(String id){
		this.idWsag=id;
	}
	public String getWsagDescription(){
		return this.wsagDescription;
	}

}
