package action;

import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.LabelValueBean;

import xml.JDOMWriter;
import entity.Wsag;
import form.StoreWsag_Form;
import db.DBWsag;
public class StoreWsag_Action extends Action {
	
	
	
	
	/**
	 * This method performs the override of the execute method of class Action and gives in output an ActionForward.
	 * It allow to login and save user in the session.   
	 *
	 * @param mapping        The ActionMapping.
	 * @param form        The ActionForm.
	 * @param request        The HttpServletRequest.
	 * @param response        The HttpServletResponse.      
	 * @return       The ActionForward
	 * @throws Exception the exception
	 */
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		
		String returnMapping="";
		String idSla = null;
		String nameSla = null;
		String descriptionSla = null;
		returnMapping="success";
		if(form!=null){
			//effettuo il cast del form in ingresso
			StoreWsag_Form wsagFrom = (StoreWsag_Form)form;
			idSla=wsagFrom.getIdSla();
			nameSla=wsagFrom.getNameSla();
			descriptionSla=wsagFrom.getDescriptionSla();
			
			System.out.println(idSla);
			System.out.println(nameSla);
			System.out.println(descriptionSla);
			if(idSla!=null){
				JDOMWriter xmlWriter=JDOMWriter.getInstance();
				//xmlWriter.configBase();
				xmlWriter.salvaNomeWsag(idSla);
				//System.err.println(xmlWriter.convertiXML());
				
				xmlWriter.convertiXML().replace("'", "£");
				DBWsag dbWsag=new DBWsag(nameSla,xmlWriter.convertiXML().replace("'", "£"));
				//DBWsag dbWsag=new DBWsag(output,StringEscapeUtils.escapeJava(xmlWriter.convertiXML()));
				//xmlWriter.pulisciTemplate();
				returnMapping="finish";
				
				
			}
			
			
		}
		
		return mapping.findForward(returnMapping);
	}

}