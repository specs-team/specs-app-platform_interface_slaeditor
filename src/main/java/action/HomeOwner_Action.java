package action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class HomeOwner_Action extends Action {
	
	/**
	 * This method performs the override of the execute method of class Action and gives in output an ActionForward.
	 * It allow to login and save user in the session.   
	 *
	 * @param mapping        The ActionMapping.
	 * @param form        The ActionForm.
	 * @param request        The HttpServletRequest.
	 * @param response        The HttpServletResponse.      
	 * @return       The ActionForward
	 * @throws Exception the exception
	 */
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		System.out.println("sono in Home Owner");
		
		return mapping.findForward("success");
	}

}
