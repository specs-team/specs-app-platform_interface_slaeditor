<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>SPECS SLA editor</title>

<link href="bootstrap/css/bootstrap.min.css" type="text/css"
	rel="stylesheet">
<style type="text/css">
body {
	padding-top: 60px;
	padding-bottom: 40px;
}

.sidebar-nav {
	padding: 9px 0;
}
</style>

</head>
<!-- ********************************-->

<body ng-app="SPECS-APP-SLA-EDITOR">

	<div id="_navigator">
		<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"> <img alt="Brand"
					src="img/specs-logo-32.png"></a>
			</div>
			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li><a
						href="<%=request.getContextPath()%>/HomeDeveloper_Action.do">Home</a></li>
					<li><a
						href="<%=request.getContextPath()%>/BrowseSecurityMetric_Action.do">View
							Security Metric</a></li>


				</ul>
			</div>
			<!--/.nav-collapse -->
		</div>
		</nav>
	</div>

	<!-- Main jumbotron for a primary marketing message or call to action -->
	<div class="panel" ng-controller="slaEditContr">
		<div class="container">

			<div class="panel panel-default" ng-show="abstractMetric">
				<div class="panel-body">
					<h1>Browse Security Metric</h1>
				</div>
				<div class="well ">
					
					<div class="panel panel-default">
						<div class="panel-body ">

							<b> METRIC</b> <br />
							
							<table class="table">
								<thead>
									<tr>
										<th width="30%">Attribute</th>
										<th width="30%">Value</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td><div>Name</div></td>
										<td><div>{{abstractMetric.name}}</div></td>
									</tr>
									<tr>
										<td><div>Reference Id</div></td>
										<td><div>{{abstractMetric.referenceId}}</div></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					
					<!--  METRIC DEFINITION -->
					<div class="panel panel-default"
						ng-show="abstractMetric.specs_metricDefinition">
						<div class="panel-body ">
							<b> METRIC DEFINITION</b> <br />

							<!-- UNIT -->
							<div ng-show="abstractMetric.specs_metricDefinition.specs_unit">
								<br /> <b>UNIT</b> <br />

								<table class="table">

									<thead>
										<tr>
											<th width="30%">Attribute</th>
											<th width="30%">Value</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td><div>Name</div></td>
											<td><div>{{abstractMetric.specs_metricDefinition.specs_unit.name}}</div></td>
										</tr>
									</tbody>
								</table>

								<div
									ng-show="abstractMetric.specs_metricDefinition.specs_unit.specs_intervalUnit">
									<br /> <b> INTERVAL UNIT</b> <br />

									<table class="table">

										<thead>
											<tr>
												<th width="30%">Element</th>
												<th width="30%">Value</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td><div>Interval Items Type</div></td>
												<td><div>{{abstractMetric.specs_metricDefinition.specs_unit.specs_intervalUnit.intervalItemsType}}</div></td>
											</tr>
											<tr>
												<td><div>Interval Item Start</div></td>
												<td><div>{{abstractMetric.specs_metricDefinition.specs_unit.specs_intervalUnit.intervalItemStart}}</div></td>
											</tr>
											<tr>
												<td><div>Interval Item Stop</div></td>
												<td><div>{{abstractMetric.specs_metricDefinition.specs_unit.specs_intervalUnit.intervalItemStop}}</div></td>
											</tr>
											<tr>
												<td><div>Interval Item Step</div></td>
												<td><div>{{abstractMetric.specs_metricDefinition.specs_unit.specs_intervalUnit.intervalItemStep}}</div></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>


							<!-- SCALE -->
							<div ng-show="abstractMetric.specs_metricDefinition.specs_scale">
								<br /> <b>SCALE</b> <br />

								<table class="table">

									<thead>
										<tr>
											<th width="30%">Type</th>
											<th width="30%">Value</th>
										</tr>
									</thead>
									<tbody>
										<tr
											ng-show="abstractMetric.specs_metricDefinition.specs_scale.specs_Quantitative">
											<td><div>Quantitative</div></td>
											<td><div>{{abstractMetric.specs_metricDefinition.specs_scale.specs_Quantitative}}</div></td>
										</tr>
										<tr
											ng-show="abstractMetric.specs_metricDefinition.specs_scale.specs_Qualitative">
											<td><div>Qualitative</div></td>
											<td><div>{{abstractMetric.specs_metricDefinition.specs_scale.specs_Qualitative}}</div></td>
										</tr>
									</tbody>
								</table>
							</div>


							<!-- EXPRESSION -->
							<div ng-show="abstractMetric.specs_metricDefinition.specs_expression">
								<br /> <b>EXPRESSION</b> <br />

								<div>{{abstractMetric.specs_metricDefinition.specs_expression}}</div>
								<br />
							</div>

							<!-- DEFINITION -->
							<div ng-show="abstractMetric.specs_metricDefinition.specs_definition">
								<br /> <b>DEFINITION</b> <br />

								<div>{{abstractMetric.specs_metricDefinition.specs_definition}}</div>
								<br />
							</div>

							<!-- NOTE -->
							<div ng-show="abstractMetric.specs_metricDefinition.note">
								<br /> <b>NOTE</b> <br />

								<div>{{abstractMetric.specs_metricDefinition.note}}</div>
								<br />
							</div>

						</div>

					</div>
					
					
		<!--  METRIC RULE DEFINITION -->
					<div class="panel panel-default"
						ng-show="abstractMetric.specs_metricRules">
						<div class="panel-body ">
							<b>METRIC RULE DEFINITION</b> <br />
							<div ng-show="abstractMetric.specs_metricRules">
								<br /> <b>RULE DEFINITION</b> <br />
								<table class="table">

									<thead>
										<tr>
											<th width="30%">Attribute</th>
											<th width="30%">Value</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td><div>Name</div></td>
											<td><div>{{abstractMetric.specs_metricRules.specs_metricRule.ruleId}}</div></td>
										</tr>
										<tr>
											<td><div>Rule value</div></td>
											<td><div>{{abstractMetric.specs_metricRules.specs_metricRule.specs_ruleValue}}</div></td>
										</tr>
									</tbody>
								</table>

								<!-- DEFINITION -->
								<div
									ng-show="abstractMetric.specs_metricRules.specs_metricRule.specs_ruleDescription">
									<b>DEFINITION</b> <br />
									<div>{{abstractMetric.specs_metricRules.specs_metricRule.specs_ruleDescription}}</div>
								</div>

								<!-- NOTE -->
								<div
									ng-show="abstractMetric.specs_metricRules.specs_metricRules.note">
									<br /> <b>NOTE</b> <br />

									<div>{{abstractMetric.specs_metricRules.specs_metricRules.note}}</div>
								</div>
								<br />
							</div>
						</div>
					</div>


					<!--  METRIC PARAMETER DEFINITION -->
					<div class="panel panel-default"
						ng-show="abstractMetric.specs_metricParameters">
						<div class="panel-body ">
							<b> METRIC PARAMETER DEFINITION</b> <br />
							<div ng-show="abstractMetric.specs_metricParameters">
								<br /> <b>1 PARAMETER DEFINITION</b> <br />
								<table class="table">

									<thead>
										<tr>
											<th width="30%">Attribute</th>
											<th width="30%">Value</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td><div>Name</div></td>
											<td><div>{{abstractMetric.specs_metricParameters.paramId}}</div></td>
										</tr>
										<tr>
											<td><div>Type</div></td>
											<td><div>{{abstractMetric.specs_metricParameters.specs_paramType}}</div></td>
										</tr>
										<tr>
											<td><div>Value</div></td>
											<td><div>{{abstractMetric.specs_metricParameters.specs_paramValue}}</div></td>
										</tr>
									</tbody>
								</table>

								<!-- DEFINITION -->
								<div
									ng-show="abstractMetric.specs_metricParameters.specs_paramDescription">
									<b>DEFINITION</b> <br />
									<div>{{abstractMetric.specs_metricParameters.specs_paramDescription}}</div>
								</div>

								<!-- NOTE -->
								<div
									ng-show="abstractMetric.specs_metricParameters.note">
									<br /> <b>NOTE</b> <br />

									<div>{{abstractMetric.specs_metricParameters.note}}</div>
								</div>

								<br />
							</div>
						</div>
					</div>









			</div>
		</div>
	</div>

	<jsp:include page="../common/footer.jsp" />


	<script src="<%=request.getContextPath()%>/js/angular.js"></script>
	<!-- *************************** -->
	<script src="<%=request.getContextPath()%>/js/jquery.min.js"></script>
	<script src="<%=request.getContextPath()%>/js/jquery.xpath.js"></script>
	<script src="<%=request.getContextPath()%>/js/jquery.xml2json.js"></script>

	<script
		src="<%=request.getContextPath()%>/js/bootstrap_wizard/jquery.bootstrap.wizard.js"></script>

	<!-- Page constructors -->
	<!-- ANGULAR SCRIPT -->

	<script>
		var app = angular.module('SPECS-APP-SLA-EDITOR', []);

		app.controller('slaEditContr', [ '$scope', function($scope) {
			console.log("chiamata inizializzazione angular");
			$scope.metricUserName = "";
			
			$scope.xmlRetrieved = unescape(${pippo});
			console.log($scope.xmlRetrieved);
			$scope.capabilityTemplate = $.parseXML($scope.xmlRetrieved);
		    $scope.abstractMetric = $.xml2json($scope.capabilityTemplate);
		    //findAndReplace($scope.abstractCapability,'nist','nist:');
		    /* findAndReplace($scope.abstractCapability,'nist:','nist'); */
		    //findAndReplace($scope.abstractCapability,"nist","nist:");
		    //findAndReplace($scope.abstractCapability,"nist:","nist");
		  
		    console.log($scope.abstractMetric);
 
		} ]);
	</script>

	<script>
	function findAndReplace(object, value, replacevalue){
		  for(var x in object){
		    if(typeof object[x] == 'object'){
		      findAndReplace(object[x], value, replacevalue);
		    }
		    if(object[x] == value){ 
		      object["name"] = replacevalue;
		      // break; // uncomment to stop after first replacement
		    }
		  }
		}
	</script>
</body>
</html>