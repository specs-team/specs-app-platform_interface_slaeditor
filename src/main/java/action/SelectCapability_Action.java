package action;

import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.*;
import org.apache.struts.util.LabelValueBean;

import xml.JDOMWriter;
import entity.Capability;
import form.CreateGT_Form;
import form.SelectCapability_Form;

public class SelectCapability_Action extends Action {
	
	public Collection capabilityForm=new ArrayList();
	public ArrayList<Capability> arrayListCapability= new ArrayList<Capability>();
	
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		String capabilitySelezionate=null;
		
		System.out.println("Sono in select capability action");
		Capability capability=new Capability();
		arrayListCapability=capability.catturaCapability();
		
		capabilityForm.clear();
		for(int i=0;i<arrayListCapability.size();i++){
			capabilityForm.add(new LabelValueBean(arrayListCapability.get(i).getIdCapability() , arrayListCapability.get(i).getIdCapability()));
		}
		request.setAttribute("capabilityForm", capabilityForm);
		

		if(form!=null){
		SelectCapability_Form selectCapabilityForm=(SelectCapability_Form)form;
		capabilitySelezionate=selectCapabilityForm.getSelectedItems();
		
		if(capabilitySelezionate!=null){
			System.out.println(capabilitySelezionate);
		}
		
		}
		
		
//		JDOMWriter xmlWriter=JDOMWriter.getInstance();
//		//xmlWriter.configBase();
//		xmlWriter.salvaXMLcapability();
//		
//		System.err.println(xmlWriter.convertiXML());
		
		return mapping.findForward("success");
	}

}
