package db;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import com.mysql.jdbc.Connection;

import metadata.DB;

public class DBConnectionManager {

	// attributi utili per stabilire la connesione
	private static String url = DB.url;
	private static String dbName = DB.DBName;
	private static String driver = DB.driver;
	private static String userName = DB.userName;
	private static String password = DB.password;

	// apre la connessione con il database
	private static Connection getConnection() throws Exception {
		Connection conn = null;
		
		Class.forName(driver).newInstance();
		// System.out.println("Loaded driver database");
		conn = (Connection) DriverManager.getConnection(url + dbName, userName,
				password);
		// System.out.println("Connected to MySQL");
		return conn;
	}

	// invia una query interrogativa
	public static ResultSet selectQuery(String query) throws Exception {
		Connection conn = getConnection();
		Statement statement = conn.createStatement();
		ResultSet ret = statement.executeQuery(query);
		//conn.close();
		return ret;
	}

	// invia una query di tipo UPDATE
	public static int updateQuery(String query) throws Exception {
		Connection conn = getConnection();
		Statement statement = conn.createStatement();
		int ret = statement.executeUpdate(query);
//		conn.close();
		return ret;
	}

}
