<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>SPECS SLA editor</title>

<link href="bootstrap/css/bootstrap.min.css" type="text/css"
	rel="stylesheet">
<style type="text/css">
body {
	padding-top: 60px;
	padding-bottom: 40px;
}

.sidebar-nav {
	padding: 9px 0;
}
</style>

</head>
<!-- ********************************-->

<body ng-app="SPECS-APP-SLA-EDITOR">

	<div id="_navigator">
		<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"> <img alt="Brand"
					src="img/specs-logo-32.png"></a>
			</div>
			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li><a href="<%=request.getContextPath()%>/HomeDeveloper_Action.do">Home</a></li>
					<li><a
						href="<%=request.getContextPath()%>/BrowseCapability_Action.do">View
							Capability</a></li>
					<li><a
						href="<%=request.getContextPath()%>/DeleteCapability_Action.do">Delete
							Capability</a></li>

				</ul>
			</div>
			<!--/.nav-collapse -->
		</div>
		</nav>
	</div>

	<!-- Main jumbotron for a primary marketing message or call to action -->
	<div class="panel" ng-controller="slaEditContr">
		<div class="container">


			<div class="panel panel-default" ng-show="abstractCapability">
				<div class="panel-heading">
					<h1>Capability details</h1>
				</div>
				
				<div class="panel panel-default" >
					<div class="panel-body">
						<table class="table">
							<thead>
								<br>
								<b>CAPABILITY</b>
								<br />
							
							</thead>
							<tbody>
								<tr>
									<td><div>Id Capability</div></td>
									<td><div>{{abstractCapability.id}}</div></td>
								</tr>
								<tr>
									<td><div>Name Capability</div></td>
									<td><div>{{abstractCapability.name}}</div></td>
								</tr>
								<tr>
									<td><div>Description Capability</div></td>
									<td><div>{{abstractCapability.description}}</div></td>
								</tr>
							</tbody>
						</table>
				
					</div>
				</div>
				


				<div class="panel-body" ng-show="!isSecurityControlArray">
					<div class="well">

						<table class="table">

							<thead>

								<br>
								<b>{{$index+1}} SECURITY CONTROL</b>
								<br />

								<tr>
									<th width="30%">Attribute</th>
									<th width="30%">Value</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td><div>id</div></td>
									<td><div>{{abstractCapability.security_control.id}}</div></td>
								</tr>
								<tr>
									<td><div>Name</div></td>
									<td><div>{{abstractCapability.security_control.name}}</div></td>
								</tr>
								<tr>
									<td><div>Control family id</div></td>
									<td><div>{{abstractCapability.security_control.nistcontrol_family_id}}</div></td>
								</tr>
								<tr>
									<td><div>Control family name</div></td>
									<td><div>{{abstractCapability.security_control.nistcontrol_family_name}}</div></td>
								</tr>
								<tr>
									<td><div>Security control</div></td>
									<td><div>{{abstractCapability.security_control.nistsecurity_control}}</div></td>
								</tr>
								<tr>
									<td><div>Control enhancement</div></td>
									<td><div>{{abstractCapability.security_control.nistcontrol_enhancement}}</div></td>
								</tr>

							</tbody>
						</table>
						<div>
							<b>Description</b> <br />
							<div>{{abstractCapability.security_control.control_description}}</div>
						</div>
					</div>
				</div>
				<div class="panel-body" ng-show="isSecurityControlArray"
					ng-repeat="securityControl in abstractCapability.security_control">
					<div class="well">

						<table class="table">

							<thead>

								<br>
								<b>{{$index+1}} SECURITY CONTROL</b>
								<br />

								<tr>
									<th width="30%">Attribute</th>
									<th width="30%">Value</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td><div>id</div></td>
									<td><div>{{securityControl.id}}</div></td>
								</tr>
								<tr>
									<td><div>Name</div></td>
									<td><div>{{securityControl.name}}</div></td>
								</tr>
								<tr>
									<td><div>Control family id</div></td>
									<td><div>{{securityControl.nistcontrol_family_id}}</div></td>
								</tr>
								<tr>
									<td><div>Control family name</div></td>
									<td><div>{{securityControl.nistcontrol_family_name}}</div></td>
								</tr>
								<tr>
									<td><div>Security control</div></td>
									<td><div>{{securityControl.nistsecurity_control}}</div></td>
								</tr>
								<tr>
									<td><div>Control enhancement</div></td>
									<td><div>{{securityControl.nistcontrol_enhancement}}</div></td>
								</tr>

							</tbody>
						</table>

						<div>
							<b>Description</b> <br />
							<div>{{securityControl.control_description}}</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>

	<jsp:include page="../common/footer.jsp" />


	<script src="<%=request.getContextPath()%>/js/angular.js"></script>
	<!-- *************************** -->
	<script src="<%=request.getContextPath()%>/js/jquery.min.js"></script>
	<script src="<%=request.getContextPath()%>/js/jquery.xpath.js"></script>
	<script src="<%=request.getContextPath()%>/js/jquery.xml2json.js"></script>

	<script
		src="<%=request.getContextPath()%>/js/bootstrap_wizard/jquery.bootstrap.wizard.js"></script>

	<!-- Page constructors -->
	<!-- ANGULAR SCRIPT -->

	<script>
		var app = angular.module('SPECS-APP-SLA-EDITOR', []);

		app.controller('slaEditContr', [ '$scope', function($scope) {
			console.log("chiamata inizializzazione angular");
			$scope.metricUserName = "";
			
			$scope.xmlRetrieved = unescape(${pippo});
			console.log($scope.xmlRetrieved);
			
			
			$scope.capabilityTemplate = $.parseXML($scope.xmlRetrieved);
		    $scope.abstractCapability = $.xml2json($scope.capabilityTemplate);
		    //findAndReplace($scope.abstractCapability,'nist','nist:');
		    /* findAndReplace($scope.abstractCapability,'nist:','nist'); */
		    //findAndReplace($scope.abstractCapability,"nist","nist:");
		    //findAndReplace($scope.abstractCapability,"nist:","nist");
		  
		    console.log($scope.abstractCapability);
		    
		    if ($scope.abstractCapability.security_control instanceof Array) {
				$scope.isSecurityControlArray = true;
			} else {
				$scope.isRuleDefinitionArray = false;
			}
		    
		    
		    
		} ]);
	</script>

	<script>
	function findAndReplace(object, value, replacevalue){
		  for(var x in object){
		    if(typeof object[x] == 'object'){
		      findAndReplace(object[x], value, replacevalue);
		    }
		    if(object[x] == value){ 
		      object["name"] = replacevalue;
		      // break; // uncomment to stop after first replacement
		    }
		  }
		}
	</script>
</body>

</html>