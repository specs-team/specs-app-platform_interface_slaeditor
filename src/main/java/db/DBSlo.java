package db;

import java.sql.ResultSet;
import java.util.ArrayList;

import metadata.DB;

public class DBSlo {
	
	private String idMetric;
	private String idSLO;
	private String operator;
	private String operand;
	private String importance;
	
	public DBSlo(){
	}
	
	public ArrayList catturaSLO(String[] idMetric){
		ArrayList arrayList= new ArrayList();
		boolean fine=false;
		
		String query="SELECT * FROM "+DB.DBName+"."+DB.slo+" where idMetric=";
		for(int i=0;i<idMetric.length;i++){
			query=query+"'"+idMetric[i]+"'";
			if(i==idMetric.length-1){
				fine=true;
			}
			System.out.println(fine);
			if(!fine){
				query=query+" OR idMetric=";
			}
		}
		System.out.println(query);
		try {			
			ResultSet rs = DBConnectionManager.selectQuery(query);
			while(rs.next()) {
				arrayList.add(rs.getString("idMetric"));
				arrayList.add(rs.getString("idSlo"));
				arrayList.add(rs.getString("operator"));
				arrayList.add(rs.getString("operand"));
				arrayList.add(rs.getString("importance"));
			}
			} catch(Exception e){
				e.printStackTrace();
		}
		return arrayList;
	}
	
		

}
